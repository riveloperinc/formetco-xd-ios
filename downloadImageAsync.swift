//
//  ImageLoader.swift
//  extension
//
//  Created by Nate Lyman on 7/5/14.
//  Copyright (c) 2014 NateLyman.com. All rights reserved.
//
import UIKit
import Foundation

class ImageLoader {
    
    var cache = NSCache<AnyObject, AnyObject>()
    
    class var sharedLoader : ImageLoader {
        struct Static {
            static let instance : ImageLoader = ImageLoader()
        }
        return Static.instance
    }
    
    func getDataFromUrl(_ urL:URL, completion: @escaping ((_ data: Data?) -> Void)) {
        URLSession.shared.dataTask(with: urL, completionHandler: { (data, response, error) in
            completion(NSData(data: data!) as Data)
            }) .resume()
    }
    
    func downloadImage(_ url:URL){

    }

    
    func imageForUrl(_ urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
    
        /*if let data = self.cache.objectForKey(urlString) as? NSData {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                autoreleasepool{
                    let image = UIImage(data: data)
                    dispatch_async(dispatch_get_main_queue()) {
                        completionHandler(image: image, url: urlString)
                    }
                }
            }
            return
        }*/
        
        let downloadTask = URLSession.shared.dataTask(with: URL(string: urlString)!, completionHandler: { data, response, error in
            
            // report basic networking errors
            
            if data == nil {
                print("\(urlString) error \(error)")
                DispatchQueue.main.async {
                    completionHandler(nil, urlString)
                }
                return
            }
            
            // report HTTP errors
            
            if let httpResponse = response as? HTTPURLResponse {
                let statusCode = httpResponse.statusCode
                if statusCode != 200 {
                    print("\(urlString): statusCode = \(statusCode)")
                    DispatchQueue.main.async {
                        completionHandler(nil, urlString)
                    }
                    return
                }
            }
            
            // no HTTP error, let's try to get image
            autoreleasepool{
                if let image = UIImage(data: data!) {
                    //self.cache.setObject(data, forKey: urlString)
                    autoreleasepool{
                        DispatchQueue.main.async {
                            completionHandler(image, urlString)
                        }
                    }
                } else {
                    print("\(urlString) not valid image")  // should never get here
                    DispatchQueue.main.async {
                        completionHandler(nil, urlString)
                    }
                }
            }
        }) 
        
        downloadTask.resume()
    }
}

