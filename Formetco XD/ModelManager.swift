//
//  ModelManager.swift
//  Formetco XD
//
//  Created by Austin Sweat on 5/4/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

class ModelManager{
    var email : String = ""
    var token : String = ""
    var fromHome : Bool = false
    var fromLogin : Bool = false
    var xamineUrl : String = ""
    var xperienceTitle : String = "Formetco Campus Tour"
    var xperienceName : String = ""
    var xperienceEmail : String = ""
    var xperienceOrg : String = ""
    var xperiencePhone : String = ""
    var xperienceMinute : Int = 1
    var xperienceHour : Int = 1
    var xperienceTimeOfDay : Int = 1
    var xperienceDate : Date = Date()
    
    var xplainVideos : [Xplain] = []
    var xtras : [Xtra] = []
    var xamples : [JSON] = []
    
    static var storyboard : UIStoryboard = UIStoryboard()
    
    fileprivate struct Static {
        
        static var instance:ModelManager?
        
    }
    
    func getXplainPages() -> [XplainPage] {
        var tmp_pages : [XplainPage] = []
        var tmp_page : XplainPage = XplainPage(ID: 0, XplainVideos: [])
        var tmp_videos : [Xplain] = []
        var tmp_page_index = 0
        var tmp_video_index = 0
        
        var t = 6
        
        for video in xplainVideos{
            
            if (tmp_video_index == t){
                tmp_page = XplainPage(ID: tmp_page_index, XplainVideos: tmp_videos)
                tmp_page_index = tmp_page_index + 1
                tmp_pages.append(tmp_page)
                t += 6
            }else{
                tmp_videos.append(video)
                tmp_video_index += 1
            }
        }
        
        return tmp_pages
    }
    
    func isLoggedIn() -> Bool{
        if (email != "" && token != ""){
            return true
        }else{
            return false
        }
    }
    
    class var sharedInstance:ModelManager {
        
        if Static.instance == nil {
            
            Static.instance = ModelManager()
            
        }
        
        return Static.instance!
        
    }
    
    func getXampleImageLink(_ id: Int!, imageID: Int!) -> String{
        let baseURL = "http://www.formetco.com/app/xamples/"
        return "\(baseURL)\(determineXample(id).folderName)/\(imageID).jpg"
    
    }
    
    func determineXample(_ id: Int!) -> Xample{
        var folderName : String = ""
        var properName : String = ""
        switch(id){
            case 1:
                properName = "Weather Conditions"
                folderName = "weather_forecasts"
                break
            
            case 2:
                properName = "Live Data (RSS)"
                folderName = "live_data"
                break
            
            case 3:
                properName = "Countdowns"
                folderName = "timed_countdowns"
                break
            
            case 4:
                properName = "Photo Support"
                folderName = "photo_support"
                break
            
            case 5:
                properName = "Time & Temp"
                folderName = "time_and_temp"
                break
            
            case 6:
                properName = "Conditional Updates"
                folderName = "conditional_update"
                break
        
            case 7:
                properName = "Social Media"
                folderName = "social_media"
                break
            
            case 8:
                properName = "Public Service"
                folderName = "public_service"
                break
            
            default:
                properName = "Xample Type"
                folderName = "blank"
                break
        }
        
        return Xample(id: id, folderName: folderName, properName: properName)
    }
}

struct Xample {
    var ID : Int!
    var folderName : String!
    var properName : String!
    
    init(id: Int!, folderName: String!, properName: String!){
        self.ID = id
        self.folderName = folderName
        self.properName = properName
    }
}

struct Xtra {
    var fileName : String!
    var properName : String!
    var locked : Bool!
    init(fileName: String!, properName: String!, locked: Bool!){
        self.fileName = fileName
        self.properName = properName
        self.locked = locked
    }
}

struct Xplain {
    var ID : Int!
    var youtubeTitle : String!
    var youtubeDescription : String!
    var youtubeID : String!
    
    init(ID: Int!, youtubeTitle: String!, youtubeDescription: String!, youtubeID: String!){
        self.ID = ID
        self.youtubeID = youtubeID
        self.youtubeTitle = youtubeTitle
        self.youtubeDescription = youtubeDescription
    }
}

struct XplainPage {
    var ID : Int!
    var XplainYoutubeVideos : [Xplain]!
    
    init(ID: Int!, XplainVideos: [Xplain]){
        self.ID = ID
        self.XplainYoutubeVideos = XplainVideos
    }
    
}

extension UINavigationController{
    override func forceRotateToPortrait(){
        if (UIDevice.current.orientation != UIDeviceOrientation.portrait){
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }
    
    override func forceRotateToLandscapeRight(){
        if (UIDevice.current.orientation != UIDeviceOrientation.landscapeLeft && UIDevice.current.orientation != UIDeviceOrientation.landscapeRight){
            let value = UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }
}

extension UIViewController{
    func forceRotateToPortrait(){
        if (UIDevice.current.orientation != UIDeviceOrientation.portrait){
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }
    
    func forceRotateToLandscapeRight(){
        if (UIDevice.current.orientation != UIDeviceOrientation.landscapeLeft && UIDevice.current.orientation != UIDeviceOrientation.landscapeRight){
            let value = UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }
}

extension UINavigationController{
    override func dispose(){
        /*self.view.removeAllSubviews()
        print("Called dispose")
        for sv in view.subviews {
            sv.removeFromSuperview()
        }*/
    }
}

extension UIViewController{
    func dispose(){
        /*self.view.removeAllSubviews()
        print("Called dispose")
        for sv in view.subviews {
            sv.removeFromSuperview()
        }*/
    }
    
    func moveDown(){
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            //self.view.frame.origin.y = self.navigationController!.navigationBar.frame.height
            self.navigationController?.navigationBar.backgroundColor = UIColor.black
            self.navigationController?.navigationBar.barStyle = UIBarStyle.black
            self.navigationController?.navigationBar.barTintColor = UIColor.black
            self.navigationController?.navigationBar.isTranslucent = false
            let logoView = UIImageView(image: UIImage(named: "Logo")!)
            logoView.isUserInteractionEnabled = true
        }
    }
}
