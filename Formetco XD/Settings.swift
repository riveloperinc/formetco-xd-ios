//
//  Settings.swift
//  Formetco
//
//  Created by Austin Sweat on 3/2/15.
//  Copyright (c) 2015 Formetco. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Settings{
    init(){
        
    }
    
    func getSettings() -> AnyObject{
        var myDict: NSMutableDictionary?
        if let path = Bundle.main.path(forResource: "Properties", ofType: "plist") {
            myDict = NSMutableDictionary(contentsOfFile: path)
        }
        if let dict = myDict {
            return dict
        }else{
            return 3 as AnyObject
        }
    }
    
    func getEmail() -> String{
        var settingDict: NSMutableDictionary?
        if let path = Bundle.main.path(forResource: "Properties", ofType: "plist") {
            settingDict = NSMutableDictionary(contentsOfFile: path)
            print("Email Available")
            return settingDict?.value(forKey: "email") as! String
        }else{
            print("No Email Available")
            return ""
        }
    }
    
    func setEmail(_ email: String) -> Void{
        var settingDict: NSMutableDictionary?
        if let path = Bundle.main.path(forResource: "Properties", ofType: "plist") {
            settingDict = NSMutableDictionary(contentsOfFile: path)
            settingDict?.setObject(email, forKey: "email" as NSCopying)
            settingDict?.write(toFile: path, atomically: false)
            print("Email Set")
        }
    }
    
    func getAPIUrl() -> String{
        let settings = self.getSettings() as! NSMutableDictionary
        return settings.value(forKey: "apiurl") as! String
    }
}
