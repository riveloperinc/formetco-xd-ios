//
//  XamineViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import MessageUI
import AudioToolbox
import AVFoundation

@available(iOS 8.0, *)
class XamineViewController : UIViewController, UINavigationControllerDelegate, UIAlertViewDelegate{
    @IBOutlet weak var cameraWebView: UIWebView!
    @IBOutlet weak var errorContainerView: UIView!
    var audioPlayer = AVAudioPlayer()
    override func viewDidLoad() {
        autoreleasepool {
            if (Reachability.isConnectedToNetwork() == false){
                errorContainerView.isHidden = false
            }else{
                if (ModelManager.sharedInstance.isLoggedIn() == false){
                    ModelManager.sharedInstance.fromLogin = true
                    /*Present login, then after dismissed open pdf file if they're then logged in*/
                    let loginViewController = self.storyboard!.instantiateViewController(withIdentifier: "loginview") as? LoginViewController
                    let navigationController = UINavigationController(rootViewController: loginViewController!)
                    navigationController.delegate = self
                    //var navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: Selector(("dismissView:")))
                    navigationController.navigationBar.barStyle = UIBarStyle.black
                    navigationController.navigationBar.isTranslucent = true
                    navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
                    //loginViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
                    self.present(navigationController, animated: true, completion: nil)
                }
                self.loadRequest()
            }
        }
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamineViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        self.moveDown()
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func startRequestTimer(){
        autoreleasepool {
            let seconds = 6.0
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                self.loadRequest()
            })
        }
    }
    
    func loadRequest(){
        autoreleasepool {
            if (ModelManager.sharedInstance.isLoggedIn()){ /*Decepated check with Xamine in ModelManager*/
                let url : String = "\(Settings().getAPIUrl())?_app=xd&cmd=xamine&token=" + ModelManager.sharedInstance.token
                let request : NSMutableURLRequest = NSMutableURLRequest()
                request.url = URL(string: url)
                request.httpMethod = "GET"
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                    if data != nil {
                        //self.cameraWebView.hidden = true
                        var jObj : JSON = (JSON(data: data!))["link"]
                        ModelManager.sharedInstance.xamineUrl = jObj.stringValue
                    
                        let url : URL = URL(string: ModelManager.sharedInstance.xamineUrl)!
                        let request : URLRequest = URLRequest(url: url)
                        self.cameraWebView.loadRequest(request)
                        let seconds = 8.0
                        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
                        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
                    
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                            self.progressBar.isHidden = true
                            self.progressBar.stopAnimating()
                            self.startRequestTimer()
                        })
                    }
                }
            }
        }
    }
    
    var request : NSMutableURLRequest = NSMutableURLRequest()
    
    override func viewDidAppear(_ animated: Bool) {
        autoreleasepool {
            self.progressBar.isHidden = false
            self.progressBar.startAnimating()
            //self.cameraWebView = UIWebView(frame: self.cameraWebView.frame)
            self.loadRequest()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        request = NSMutableURLRequest()
    }
    
    @IBOutlet weak var refreshWebView: UIImageView!
    
    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBAction func refresh(_ sender: AnyObject) {
        self.cameraWebView.goBack()
    }
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBAction func shareWebView(_ sender: AnyObject) {
        autoreleasepool {
        let imageToSend : UIImage = self.generateImage()
        var itemsToShare : [AnyObject] = [AnyObject]()
        itemsToShare.append(imageToSend)
        let shareControllerVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
        shareControllerVC.completionHandler = {(activityType, completed:Bool) in
            if(!completed) { return }
            if (activityType != UIActivityType.saveToCameraRoll){
                UIImageWriteToSavedPhotosAlbum(imageToSend, nil, nil, nil) /*Save the image to photo gallery */
            }
            
            let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "app", ofType: "wav")!)
            print(alertSound)
            
            do {
                // Removed deprecated use of AVAudioSessionDelegate protocol
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch _ {
            }
            do {
                try AVAudioSession.sharedInstance().setActive(true)
            } catch _ {
            }
            
            var error:NSError?
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: alertSound)
            } catch let error1 as NSError {
                error = error1
                self.audioPlayer = AVAudioPlayer()
            } catch {
                fatalError()
            }
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
        }
        
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            shareControllerVC.popoverPresentationController?.sourceView = self.view
            let frame : CGRect = CGRect(x: shareBtn.frame.origin.x, y: shareBtn.frame.origin.y - 20, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height / 2) )
            shareControllerVC.popoverPresentationController?.sourceRect = frame
        }
        
        self.present(shareControllerVC, animated: true, completion: nil)
        }
    }
    
    func generateImage() -> UIImage{
        autoreleasepool {
            UIGraphicsBeginImageContextWithOptions(cameraWebView.frame.size, false, 0)
            let context : CGContext = UIGraphicsGetCurrentContext()!
            cameraWebView.layer.render(in: context)
        }
        let img : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return img
        
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        request = NSMutableURLRequest()
    }
}

class XamineNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamineViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            self.forceRotateToLandscapeRight()
        }else{
            self.forceRotateToPortrait()
        }
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //if(self.isBeingDismissed() || self.isMovingFromParentViewController()){
            self.dispose()
        //}
    }
}
