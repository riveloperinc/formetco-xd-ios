//
//  AccountSettingTableViewController.swift
//  Formetco
//
//  Created by Austin Sweat on 30/01/2015.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class AccountSettingTableViewController: UITableViewController, UITextFieldDelegate{
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmpassword: UITextField!
    @IBOutlet weak var update: UIButton!
    @IBOutlet weak var logout: UIButton!
    
    @IBOutlet weak var useridLabel: UILabel!
    @IBOutlet weak var containerView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        password.layer.cornerRadius = 5.0
        confirmpassword.layer.cornerRadius = 5.0
        update.layer.cornerRadius = 5.0
        logout.layer.cornerRadius = 5.0
        self.tableView.alwaysBounceVertical = false
        let modelManager = ModelManager.sharedInstance
        self.useridLabel.text = "User ID: " + modelManager.email
        
    }
    
    func gotoHome(){
        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeview") as! HomeViewController
        
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
    
    func gotoLogout(){
        let modelManager = ModelManager.sharedInstance
        modelManager.token = ""
        modelManager.email = ""
        gotoHome()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func displayAlert(_ title: String, content: String){
        let alert = UIAlertController(title: title, message: content, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateBtnClicked(_ sender: AnyObject) {
        print("Update Clicked")
        if password.text != confirmpassword.text { self.displayAlert("Whoops!", content: "Please double check the form above!"); return }
        let modelManager = ModelManager.sharedInstance
        let url : String = "\(Settings().getAPIUrl())?cmd=changepassword&newpassword=\(password.text)&token=\(modelManager.token)"
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if data != nil {
                let allData : JSON = JSON(data: data!)
                print(allData)
            }
        }
        self.displayAlert("Password updated!", content: "Your password has now been changed!")
    }
    
    @IBAction func logoutBtnClicked(_ sender: AnyObject) {
        print("Logout Clicked")
        gotoLogout()
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
