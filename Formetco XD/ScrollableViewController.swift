//
//  ScrollableViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 6/17/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

class ScrollableViewController : UIScrollView{
    override func awakeFromNib() {
        let subViewController = self.subviews[0] 
        self.contentSize = CGSize(width: subViewController.bounds.width, height: subViewController.bounds.height)
    }
}