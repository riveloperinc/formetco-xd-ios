//
//  AppDelegate.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/25/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //var storyboard : UIStoryboard = UIStoryboard(name: "iPad", bundle: nil)
        //var initViewController : UIViewController = storyboard.instantiateInitialViewController() as! UIViewController
        autoreleasepool {
            var storyboard : UIStoryboard!
            if (Device() == Device.iPhone4 || Device() == Device.iPhone4s){
                /*Load iPhone 4 storyboard*/
                print("Detected iPhone4", terminator: "")
                storyboard = UIStoryboard(name: "iPhone_4", bundle: nil)
            }else if(Device() == Device.iPhone5 || Device() == Device.iPhone5s || Device() == Device.iPhone5c){
                /*Load iPhone 5 storyboard*/
                print("Detected iPhone5", terminator: "")
                storyboard = UIStoryboard(name: "iPhone_5", bundle: nil)
            }else if(Device() == Device.iPhone6 || Device() == Device.iPhone6s || Device() == Device.iPhone7){
                /*Load iPhone 6 storyboard*/
                print("Detected iPhone6", terminator: "")
                storyboard = UIStoryboard(name: "iPhone_6", bundle: nil)
            }else if(Device() == Device.iPhone6Plus || Device() == Device.iPhone6sPlus || Device() == Device.iPhone7Plus){
                /*Load iPhone 6 storyboard*/
                print("Detected iPhone6 Plus", terminator: "")
                storyboard = UIStoryboard(name: "iPhone_6_plus", bundle: nil)
            }else if((Device() == Device.iPad2) || (Device() == Device.iPad3) || (Device() == Device.iPad4) || (Device() == Device.iPadAir) || (Device() == Device.iPadAir2) || (Device() == Device.iPadMini) || (Device() == Device.iPadMini2) || (Device() == Device.iPadMini3) || (Device() == Device.iPadMini4) || (Device() == .iPadPro9Inch)){
                /*Load iPad storyboard*/
                print("Detected iPad", terminator: "")
                storyboard = UIStoryboard(name: "iPad", bundle: nil)
            }else{
                print("Detected Simulator, loading specified storyboard.", terminator: "")
                storyboard = UIStoryboard(name: "iPhone_6", bundle: nil)
            }
            ModelManager.storyboard = storyboard
            let initViewController : UIViewController = storyboard.instantiateInitialViewController()!
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = initViewController
            self.window?.makeKeyAndVisible()
        }
        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) { }
    func applicationDidEnterBackground(_ application: UIApplication) { }
    func applicationWillEnterForeground(_ application: UIApplication) { }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func applicationWillTerminate(_ application: UIApplication) { }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.all
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.xxxx.ProjectName" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] 
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Background", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Formetco.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
}

