//
//  XamplesViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class XamplesViewController : UIViewController, UIAlertViewDelegate{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptioniPhone4Label: UILabel!
    @IBOutlet weak var xamplesContainerView: UIView!
    @IBOutlet weak var xamplesScrollContainer: UIScrollView!
    @IBOutlet weak var errorContainer: UIView!
    
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamplesViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            //self.xamplesScrollContainer.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            //self.xamplesScrollContainer.contentSize = CGSize(width: self.xamplesContainerView.bounds.width, height: self.xamplesContainerView.bounds.height)
            
            if (Reachability.isConnectedToNetwork() == false){
                errorContainer.isHidden = false
            }else{
                errorContainer.isHidden = true
            }
        }
        
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
        
        self.moveDown()
    }
    
    override var shouldAutorotate : Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return true
        }else{
            return true
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            let a = (UIInterfaceOrientationMask.portrait.rawValue | UIInterfaceOrientationMask.landscapeRight.rawValue)
            return UIInterfaceOrientationMask.init(rawValue: UInt(Int(a)))
        }else{
            return UIInterfaceOrientationMask.landscape
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        if (toInterfaceOrientation.isLandscape && UIDevice.current.userInterfaceIdiom == .phone){
            self.forceRotateToPortrait()
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier != "goBack" && segue.identifier != nil){
            let senderButton = sender as! UIButton
            XamplesExpandedViewController.xampleTypeID = senderButton.tag
        }
        
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
    
    
    @IBAction func goBack(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    deinit {
        print("Deinit called on Xamples")
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
}

class XamplesNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamplesViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        
        self.navigationItem.titleView = logoView;
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        
        //var tgr = UITapGestureRecognizer(target:self, action:Selector("gotoHome"))
        //logoView.addGestureRecognizer(tgr)
        
        self.navigationItem.titleView = logoView;
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.isMovingFromParentViewController || self.isBeingDismissed){
            self.dispose()
        }
    }
    
    deinit {
        self.dispose()
    }
    
    override var shouldAutorotate : Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return true
        }else{
            return true
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return UIInterfaceOrientationMask.portrait
        }else{
            return UIInterfaceOrientationMask.landscape
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        if (toInterfaceOrientation.isLandscape && UIDevice.current.userInterfaceIdiom == .phone){
            self.forceRotateToPortrait()
        }
    }
}

@available(iOS 8.0, *)
class XamplesiPhoneTableView : UITableViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier != "goBack" && segue.identifier != nil){
            let senderButton = sender as! UIButton
            XamplesExpandedViewController.xampleTypeID = senderButton.tag
            //let value = UIInterfaceOrientation.LandscapeRight.rawValue
            //UIDevice.currentDevice().setValue(value, forKey: "orientation")
        }
    }
    
    @IBAction func xampleBtnClicked(_ sender: AnyObject) {
        let senderButton = sender as! UIButton
        /*let xamplesExpandedNavViewController = self.storyboard?.instantiateViewControllerWithIdentifier("xamplesexpandednavview") as! XamplesExpandedNavigationController
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.userInteractionEnabled = true
        var tgr = UITapGestureRecognizer(target:self, action:Selector("gotoHome"))
        logoView.addGestureRecognizer(tgr)
        xamplesExpandedNavViewController.navigationBar.topItem!.titleView = logoView*/
        
        XamplesExpandedViewController.xampleTypeID = senderButton.tag
        //let value = UIInterfaceOrientation.LandscapeRight.rawValue
        //UIDevice.currentDevice().setValue(value, forKey: "orientation")
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryboard: UIStoryboard = ModelManager.storyboard
        let nav = mainStoryboard.instantiateViewController(withIdentifier: "xamplesexpandednavview") as! UINavigationController
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamplesViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        nav.navigationBar.topItem!.titleView = logoView
        
        appdelegate.window!.rootViewController = nav
    }
    
    func gotoHome(){
        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){ self.forceRotateToPortrait() }
    }
    
    override var shouldAutorotate : Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return true
        }else{
            return true
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return UIInterfaceOrientationMask.portrait
        }else{
            return UIInterfaceOrientationMask.landscape
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        if (toInterfaceOrientation.isLandscape && UIDevice.current.userInterfaceIdiom == .phone){
            self.forceRotateToPortrait()
        }
    }
}

class XamplesCell : UITableViewCell{
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var rightImage: UIImageView!
    override func awakeFromNib() {
        
    }
   
}
