//
//  RoiViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 1/24/16.
//  Copyright © 2016 Austin Sweat. All rights reserved.
//

import Foundation
import UIKit

@available(iOS 8.0, *)
class RoiViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var expenseView: UIView!
    @IBOutlet weak var constructionCostView: UIView!
    
    
    @IBOutlet weak var leftGraph: UIView!
    @IBOutlet weak var rightGraph: UIView!
    
    @IBOutlet weak var leftProgressBar: KDCircularProgress!
    @IBOutlet weak var leftProgressBoldLabel: UILabel!
    @IBOutlet weak var leftProgressCaptionLabel: UILabel!
    
    @IBOutlet weak var rightProgressBar: KDCircularProgress!
    @IBOutlet weak var rightProgressBoldLabel: UILabel!
    @IBOutlet weak var rightProgressCaptionLabel: UILabel!
    
    @IBOutlet weak var advertisersTextField: UITextField!
    @IBOutlet weak var rateCardNetTextField: UITextField!
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var occupancyTextField: UITextField!
    @IBOutlet weak var monthlyRevenueLabel: UILabel!
    
    @IBOutlet weak var propertyLeaseTextField: UITextField!
    @IBOutlet weak var commisionTextField: UITextField!
    @IBOutlet weak var utilitiesTextField: UITextField!
    @IBOutlet weak var otherCostsTextField: UITextField!
    @IBOutlet weak var monthlyExpensesLabel: UILabel!
    
    @IBOutlet weak var digitalBillboardsTextField: UITextField!
    @IBOutlet weak var structureTextField: UITextField!
    @IBOutlet weak var otherCosts2TextField: UITextField!
    @IBOutlet weak var oneTimeConstructionCostLabel: UILabel!
    
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(RoiViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(RoiViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(RoiViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        revenueView.layer.cornerRadius = 4
        revenueView.layer.masksToBounds = true
        expenseView.layer.cornerRadius = 4
        expenseView.layer.masksToBounds = true
        constructionCostView.layer.cornerRadius = 4
        constructionCostView.layer.masksToBounds = true
        
        self.navigationItem.leftBarButtonItem?.target = self
        self.navigationItem.leftBarButtonItem?.action = #selector(RoiViewController.gotoHome)
        
        self.navigationItem.rightBarButtonItem?.target = self
        self.navigationItem.rightBarButtonItem?.action = #selector(RoiViewController.showFTX(_:))
        
        self.generate()
        
        advertisersTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        rateCardNetTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        discountTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        occupancyTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        propertyLeaseTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        commisionTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        utilitiesTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        otherCostsTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        digitalBillboardsTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        structureTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        otherCosts2TextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
        
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        self.gotoHome()
    }

    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    var used = false
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = sender.userInfo {
            if ((userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                UIView.animate(withDuration: 1.0, animations: {
                    if (self.used == false){
                        self.used = true
                        self.view.frame.origin.y -= 100
                    }
                })
            }
        }
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = sender.userInfo {
            if ((userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                UIView.animate(withDuration: 1.0, animations: {
                    if (self.used){
                        self.used = false
                        self.view.frame.origin.y += 100
                    }
                })
            }
        }
    }
    
    
    @IBAction func textFieldValueChanged(_ sender: AnyObject) {
        print("Called")
        self.generate()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.generate()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.generate()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.generate()
    }
    
    
    func generate(){
        let revenue = (((convertToDouble(advertisersTextField) * convertToDouble(rateCardNetTextField))) * ((1-(convertToDouble(discountTextField) / 100))) * (convertToDouble(occupancyTextField) / 100))
        let expenses = ( (convertToDouble(propertyLeaseTextField) + ( ( convertToDouble(commisionTextField) / 100 ) * revenue ) ) + convertToDouble(utilitiesTextField) + convertToDouble(otherCostsTextField) )
        let constructionCosts = ( convertToDouble(digitalBillboardsTextField) + convertToDouble(structureTextField) + convertToDouble(otherCosts2TextField) )
        let years = Double( ( constructionCosts / ( revenue - expenses ) ) / 12 )
        
        monthlyRevenueLabel.text = "$" + String(revenue)
        monthlyExpensesLabel.text = "$" + String(expenses)
        oneTimeConstructionCostLabel.text = "$" + String(constructionCosts)
        leftProgressBoldLabel.text = "$" + String( Int( revenue - expenses ) )
        rightProgressBoldLabel.text = String(format: "%.2f", years)
        
        let firstPercent = (Double(Int.random(Range(75...100))) * 0.01)
        
        leftProgressBar.startAngle = -90
        leftProgressBar.angle = (360 * firstPercent)
        
        rightProgressBar.startAngle = -90
        rightProgressBar.angle = (360 * firstPercent)
        
        
    }
    
    func convertToDouble(_ textField: UITextField) -> Double{
        if (textField.text == nil || textField.text == ""){
            return Double(0.00)
        }
        
        return Double(textField.text!)!
    }
}

@available(iOS 8.0, *)
class iPhoneRoiViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var expenseView: UIView!
    @IBOutlet weak var constructionCostView: UIView!
    
    @IBOutlet weak var leftGraph: UIView!
    @IBOutlet weak var rightGraph: UIView!
    
    @IBOutlet weak var leftProgressBar: KDCircularProgress!
    @IBOutlet weak var leftProgressBoldLabel: UILabel!
    @IBOutlet weak var leftProgressCaptionLabel: UILabel!
    
    @IBOutlet weak var rightProgressBar: KDCircularProgress!
    @IBOutlet weak var rightProgressBoldLabel: UILabel!
    @IBOutlet weak var rightProgressCaptionLabel: UILabel!
    
    @IBOutlet weak var advertisersTextField: UITextField!
    @IBOutlet weak var rateCardNetTextField: UITextField!
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var occupancyTextField: UITextField!
    @IBOutlet weak var monthlyRevenueLabel: UILabel!
    
    @IBOutlet weak var propertyLeaseTextField: UITextField!
    @IBOutlet weak var commisionTextField: UITextField!
    @IBOutlet weak var utilitiesTextField: UITextField!
    @IBOutlet weak var otherCostsTextField: UITextField!
    @IBOutlet weak var monthlyExpensesLabel: UILabel!
    
    @IBOutlet weak var digitalBillboardsTextField: UITextField!
    @IBOutlet weak var structureTextField: UITextField!
    @IBOutlet weak var otherCosts2TextField: UITextField!
    @IBOutlet weak var oneTimeConstructionCostLabel: UILabel!
    
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(RoiViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(RoiViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(RoiViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        revenueView.layer.cornerRadius = 4
        revenueView.layer.masksToBounds = true
        expenseView.layer.cornerRadius = 4
        expenseView.layer.masksToBounds = true
        constructionCostView.layer.cornerRadius = 4
        constructionCostView.layer.masksToBounds = true
        
        self.navigationItem.leftBarButtonItem?.target = self
        self.navigationItem.leftBarButtonItem?.action = #selector(RoiViewController.gotoHome)
        
        self.navigationItem.rightBarButtonItem?.target = self
        self.navigationItem.rightBarButtonItem?.action = #selector(RoiViewController.showFTX(_:))
        
        self.generate()
        
        advertisersTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        rateCardNetTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        discountTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        occupancyTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        propertyLeaseTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        commisionTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        utilitiesTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        otherCostsTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        digitalBillboardsTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        structureTextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
        otherCosts2TextField.addTarget(self, action: #selector(RoiViewController.textFieldValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
        
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    var used = false
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = sender.userInfo {
            if ((userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                UIView.animate(withDuration: 1.0, animations: {
                    if (self.used == false){
                        self.used = true
                        self.view.frame.origin.y -= 100
                    }
                })
            }
        }
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = sender.userInfo {
            if ((userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                UIView.animate(withDuration: 1.0, animations: {
                    if (self.used){
                        self.used = false
                        self.view.frame.origin.y += 100
                    }
                })
            }
        }
    }
    
    
    @IBAction func textFieldValueChanged(_ sender: AnyObject) {
        print("Called")
        self.generate()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.generate()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.generate()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.generate()
    }
    
    
    func generate(){
        let revenue = (((convertToDouble(advertisersTextField) * convertToDouble(rateCardNetTextField))) * ((1-(convertToDouble(discountTextField) / 100))) * (convertToDouble(occupancyTextField) / 100))
        let expenses = ( (convertToDouble(propertyLeaseTextField) + ( ( convertToDouble(commisionTextField) / 100 ) * revenue ) ) + convertToDouble(utilitiesTextField) + convertToDouble(otherCostsTextField) )
        let constructionCosts = ( convertToDouble(digitalBillboardsTextField) + convertToDouble(structureTextField) + convertToDouble(otherCosts2TextField) )
        let years = Double( ( constructionCosts / ( revenue - expenses ) ) / 12 )
        
        monthlyRevenueLabel.text = "$" + String(revenue)
        monthlyExpensesLabel.text = "$" + String(expenses)
        oneTimeConstructionCostLabel.text = "$" + String(constructionCosts)
        leftProgressBoldLabel.text = "$" + String( Int( revenue - expenses ) )
        rightProgressBoldLabel.text = String(format: "%.2f", years)
        
        let firstPercent = (Double(Int.random(Range(75...100))) * 0.01)
        
        leftProgressBar.startAngle = -90
        leftProgressBar.angle = ((360 * firstPercent))
        
        rightProgressBar.startAngle = -90
        rightProgressBar.angle = ((360 * firstPercent))
        
    }
    
    func convertToDouble(_ textField: UITextField) -> Double{
        if (textField.text == nil || textField.text == ""){
            return Double(0.00)
        }
        var a = Double(0.00)
        do{
            a = try Double(textField.text!)!
        }catch let error as exception{
            print(error)
            a = 0
        }
        return a
    }
}

extension Int
{
    static func random(_ range: Range<Int> ) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0   // allow negative ranges
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
}
