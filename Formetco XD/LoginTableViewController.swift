//
//  LoginTableViewController.swift
//  Formetco
//
//  Created by Austin Sweat on 30/01/2015.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import CoreData

class LoginTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var forgotbutton: UIButton!
    @IBOutlet weak var rememberme: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userid: UITextField!
    @IBOutlet weak var rememberMeBtn: UIButton!
    
    var checked : Bool = false
    let checkedChar: String = "\u{2611}"
    let uncheckedChar: String = "\u{2610}"
    
    @IBAction func rememberMeClicked(sender: AnyObject) {
        if checked{
            checked = false
            rememberMeBtn.setTitle("", forState: UIControlState.Normal)
        }else{
            checked = true
            rememberMeBtn.setTitle(checkedChar, forState: UIControlState.Normal)
        }
    }
    
    @IBOutlet var tableViewContainer: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var appDelegate : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context : NSManagedObjectContext = appDelegate.managedObjectContext!
        
        var request : NSFetchRequest = NSFetchRequest(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        var results : NSArray = context.executeFetchRequest(request, error: nil)!
        if(results.count > 0){
            var result : NSManagedObject = results[results.count-1] as! NSManagedObject //Final result
            userid.text = result.valueForKey("username") as! String
            password.text = result.valueForKey("password") as! String
            
            checked = true
            rememberMeBtn.setTitle(checkedChar, forState: UIControlState.Normal)
        }
        
        password.layer.cornerRadius = 5.0
        userid.layer.cornerRadius = 5.0
        loginbutton.layer.cornerRadius = 5.0
        forgotbutton.layer.cornerRadius = 5.0
        rememberme.layer.cornerRadius = 5.0
        //password.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        
        var backgroundView : UIView = UIView(frame: CGRect.zeroRect)
        backgroundView.backgroundColor = UIColor.clearColor()
        
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.backgroundView = backgroundView
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 0))?.contentView.backgroundColor = UIColor.clearColor()
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 0))?.contentView.layer.cornerRadius = 5.0
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 0))?.backgroundView = backgroundView
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 1))?.contentView.backgroundColor = UIColor.clearColor()
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 1))?.backgroundView = backgroundView
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 2))?.contentView.backgroundColor = UIColor.clearColor()
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 2))?.backgroundView = backgroundView
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 3))?.contentView.backgroundColor = UIColor.clearColor()
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 3))?.backgroundView = backgroundView
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 4))?.contentView.backgroundColor = UIColor.clearColor()
        self.tableView.cellForRowAtIndexPath(NSIndexPath(index: 4))?.backgroundView = backgroundView
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if identifier == "policyviewsegue"{
            return false
        }else{
            return false
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        /*Make API Call*/
        //let newVC = segue.destinationViewController as UIViewController
    }

    @IBAction func policyClicked(sender: AnyObject) {
        println("Policy Clicked")
        let loginController = self.parentViewController as! LoginViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let policyViewController = storyboard.instantiateViewControllerWithIdentifier("policyview") as! privicyPolicy
        
        loginController.navigationController?.pushViewController(policyViewController, animated: true)
    }
    
    func deleteLoginEntries(){
        var appDelegate : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context : NSManagedObjectContext = appDelegate.managedObjectContext!
        
        var dbrequest : NSFetchRequest = NSFetchRequest(entityName: "Users")
        dbrequest.returnsObjectsAsFaults = false
        
        var results : NSArray = context.executeFetchRequest(dbrequest, error: nil)!
        if(results.count > 0){
            for r in results{
                var result = r as! NSManagedObject
                context.deleteObject(result)
            }
            context.save(nil)
        }
    }
    
    @IBAction func loginClicked(sender: AnyObject) {
        let modelmanager = ModelManager.sharedInstance
        
        var email = userid.text!
        var password = self.password.text!
        
        if (checked || rememberMeBtn.titleLabel?.text != "") {
            self.deleteLoginEntries()
            var appDelegate : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            var context : NSManagedObjectContext = appDelegate.managedObjectContext!
            var newUser = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: context) as! NSManagedObject
            newUser.setValue(email, forKey: "username")
            newUser.setValue(password, forKey: "password")
            context.save(nil)
        }
        
        var url : String = "\(Settings().getAPIUrl())?cmd=login&email="+email+"&password="+password
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "GET"
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            if data != nil {
                var allData : JSON = JSON(data: data!)
                var Token : JSON = allData["Token"]
                println(allData)
                if Token.stringValue != "Invalid"{
                    modelmanager.token = Token.stringValue
                    modelmanager.email = email
                    self.navigationController?.popViewControllerAnimated(true)
                }else{
                    /*Present error*/
                    var alert = UIAlertController(title: "Incorrect Login!", message: "Please try again!", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func forgotClicked(sender: AnyObject) {
        let modelmanager = ModelManager.sharedInstance
        
        var email = userid.text!
        
        var url : String = "\(Settings().getAPIUrl())?cmd=resetpassword&version=2&email="+email
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "GET"
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            if data != nil {
                var allData : JSON = JSON(data: data!)
                var Success : JSON = allData["Success"]
                var Error : JSON = allData["Error"]
                if Success.stringValue != ""{
                    var alert = UIAlertController(title: "Password Reset Sent.", message: "Please check your email for your new password.", preferredStyle:UIAlertControllerStyle.Alert)
                    
                    var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    var alert = UIAlertController(title: "Invalid Email!", message: "Please check the form above, and try again.", preferredStyle:UIAlertControllerStyle.Alert)
                    
                    var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        var backgroundView : UIView = UIView(frame: CGRect.zeroRect)
        backgroundView.backgroundColor = UIColor.clearColor()
        cell.backgroundView = backgroundView
        cell.backgroundColor = UIColor.clearColor()
    }
}
