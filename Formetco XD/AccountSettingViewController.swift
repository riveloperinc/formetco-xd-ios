//
//  AccountSettingViewController.swift
//  Formetco
//
//  Created by Austin Sweat on 30/01/2015.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import MessageUI

@available(iOS 8.0, *)
class AccountSettingViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(AccountSettingViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        //_ = ModelManager.sharedInstance
        //let redColor = UIColor(red: 195.0/255.0, green: 20.0/255.0, blue: 30.0/255.0, alpha: 1.0)
        
        containerView.backgroundColor = UIColor.clear
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        self.gotoHome()
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return UIInterfaceOrientation.portrait
        }else{
            return UIInterfaceOrientation.landscapeRight
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("Call Pressed")
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
