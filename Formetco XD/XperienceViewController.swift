//
//  XperienceViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import EventKit
import CVCalendar

@objc protocol CurrencySelectedDelegate {
    func currencySelected(_ currName: String)
}

@objc protocol CurrentSelectedHourDelegate {
    func currentSelectedHour(_ currentHour: Int)
}

@objc protocol CurrentSelectedMinuteDelegate {
    func currentSelectedMinute(_ currentMinute: Int)
}

@objc protocol CurrentSelectedTimeOfDayDelegate {
    func currentSelectedTimeOfDay(_ currentTimeOfDay: String)
}

class XperienceHourPicker : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate, UIToolbarDelegate{
    @IBOutlet weak var hourPicker: UIPickerView!
    weak var delegate: CurrentSelectedHourDelegate?
    //@IBOutlet weak var teamPicker: UIPickerView!
    
    var hours = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hours.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(hours[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*Did select action here.*/
        print("Changed Position")
        delegate?.currentSelectedHour(hours[row])
    }
}

class XperienceMinutePicker : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate{
    @IBOutlet weak var hourPicker: UIPickerView!
    weak var delegate: CurrentSelectedMinuteDelegate?
    //@IBOutlet weak var teamPicker: UIPickerView!
    
    var minutes = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return minutes.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(minutes[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*Did select action here.*/
        print("Changed Position")
        delegate?.currentSelectedMinute(minutes[row])
    }
}

class XperienceTimeOfDayPicker : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate{
    @IBOutlet weak var hourPicker: UIPickerView!
    weak var delegate: CurrentSelectedTimeOfDayDelegate?
    //@IBOutlet weak var teamPicker: UIPickerView!
    
    var timesOfDay = ["AM", "PM"]
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timesOfDay.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(timesOfDay[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*Did select action here.*/
        print("Changed Position")
        delegate?.currentSelectedTimeOfDay(timesOfDay[row])
    }
}

@available(iOS 8.0, *)
class XperienceViewController : UIViewController, UITableViewDelegate, CurrentSelectedHourDelegate, CurrentSelectedMinuteDelegate, CurrentSelectedTimeOfDayDelegate, UITextFieldDelegate, UIAlertViewDelegate{
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var hourField: UITextField!
    @IBOutlet weak var minuteField: UITextField!
    @IBOutlet weak var timeOfDayField: UITextField!
    
    override func viewDidLoad() {
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){ overlayImageView.layer.opacity = 0.8
        }
        
        if (submitBtn != nil){
            submitBtn.layer.borderColor = UIColor.white.cgColor
            submitBtn.layer.borderWidth = 1
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(XperienceViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(XperienceViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);

        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XperienceViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        self.moveDown()
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    var activeTextField : UITextField = UITextField()
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField.tag == 9){
            if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
                self.selectedTextField = textField
                self.hourField.tintColor = UIColor.white
                self.hourField.inputView = nil
                self.hourField.reloadInputViews()
                createPicker(self.hourField)
                
            }
        }else if (textField.tag == 10){
            if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
                self.selectedTextField = textField
                self.minuteField.tintColor = UIColor.white
                self.minuteField.inputView = nil
                self.minuteField.reloadInputViews()
                createPicker(self.minuteField)
                
            }
        }else if (textField.tag == 11){
            if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
                self.selectedTextField = textField
                self.timeOfDayField.tintColor = UIColor.white
                self.timeOfDayField.inputView = nil
                self.timeOfDayField.reloadInputViews()
                createPicker(self.timeOfDayField)
                
            }
        }else{
            UIView.animate(withDuration: 1.0, animations: {
                self.view.frame.origin.y = textField.frame.origin.y
            })
        }
        
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    
    func keyboardWillShow(_ sender: Notification) {
        //if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad){
            //UIView.animateWithDuration(1.0, animations: {
            //    self.view.frame.origin.y -= 250
            //})
        //}
    }
    
    func keyboardWillHide(_ sender: Notification) {
        //if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad){
            //UIView.animateWithDuration(1.0, animations: {
            //    self.view.frame.origin.y += 250
            //})
        //}
    }
    
    func insertEvent(_ store: EKEventStore) {
        // 1
        let calendars = store.calendars(for: EKEntityType.event)
            
        for calendar in calendars {
            // 2
                // 3
            let startDate_ = ModelManager.sharedInstance.xperienceDate
            
            
            let calendarCur = Calendar.current
            
            let year = calendarCur.component(.year, from: startDate_)
            let month = calendarCur.component(.month, from: startDate_)
            let day = calendarCur.component(.day, from: startDate_)
            
            let components = DateComponents()
            (components as NSDateComponents).setValue(month, forComponent: NSCalendar.Unit.month)
            (components as NSDateComponents).setValue(day, forComponent: NSCalendar.Unit.day)
            (components as NSDateComponents).setValue(year, forComponent: NSCalendar.Unit.year)
            if (ModelManager.sharedInstance.xperienceTimeOfDay == 2){
                (components as NSDateComponents).setValue(ModelManager.sharedInstance.xperienceHour + 12, forComponent: NSCalendar.Unit.hour)
            }else{
                (components as NSDateComponents).setValue(ModelManager.sharedInstance.xperienceHour, forComponent: NSCalendar.Unit.hour)
            }
            (components as NSDateComponents).setValue(ModelManager.sharedInstance.xperienceMinute, forComponent: NSCalendar.Unit.minute)
            let date: Foundation.Date = Foundation.Date()
            let startDate = (Calendar.current as NSCalendar).date(byAdding: components, to: date, options: NSCalendar.Options(rawValue: 0))
                // 1 hours
            let endDate = startDate!.addingTimeInterval(1 * 60 * 60)
            
                // 4
                // Create Event
                let event = EKEvent(eventStore: store)
                event.calendar = calendar
                
                event.title = ModelManager.sharedInstance.xperienceTitle
                event.startDate = startDate!
                event.endDate = endDate
                
                // 5
                // Save Event in Calendar
                var error: NSError?
                let result: Bool
                do {
                    try store.save(event, span: EKSpan.thisEvent)
                    result = true
                } catch let error1 as NSError {
                    error = error1
                    result = false
                }
                
                if result == false {
                    if let theError = error {
                        print("An error occured \(theError)")
                    }
                }
            }
        
    }
    
    @IBAction func submitBtnClicked(_ sender: AnyObject) {
        let currentDate = CVDate(date: Foundation.Date())
        let scheudleDate = ModelManager.sharedInstance.xperienceDate
        
        let calendarCur = Calendar.current
        
        let year = calendarCur.component(.year, from: scheudleDate)
        let month = calendarCur.component(.month, from: scheudleDate)
        let day = calendarCur.component(.day, from: scheudleDate)
        
        print("ModelManagerName: \(ModelManager.sharedInstance.xperienceName)", terminator: "")
        print("ModelManagerOrg: \(ModelManager.sharedInstance.xperienceOrg)", terminator: "")
        print("ModelManagerEmail: \(ModelManager.sharedInstance.xperienceEmail)", terminator: "")
        print("ModelManagerPhone: \(ModelManager.sharedInstance.xperiencePhone)", terminator: "")
        
        if (ModelManager.sharedInstance.xperienceName == "" || ModelManager.sharedInstance.xperienceOrg == "" || ModelManager.sharedInstance.xperienceEmail == "" ||
            ModelManager.sharedInstance.xperiencePhone == ""){
            let alert = UIAlertController(title: "Whoops!", message: "Please check the form above, and try again!", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if((currentDate.year > year) ||
            (currentDate.month > month && currentDate.year >= year) ||
            (currentDate.day > day && currentDate.month >= month)){
                let alert = UIAlertController(title: "Sorry", message: "Our Xperts have yet to perfect the Flux Capacitor. Until then please select a date after (today's date).", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                }
                
                alert.addAction(okAction)
                
                self.present(alert, animated: true, completion: nil)
                return
        }else{
        
        // 1
            let eventStore = EKEventStore()
        
        // 2
            switch EKEventStore.authorizationStatus(for: EKEntityType.event) {
                case .authorized:
                    insertEvent(eventStore)
                case .denied:
                    print("Access denied")
                case .notDetermined:
            // 3
                    eventStore.requestAccess(to: EKEntityType.event, completion: {[weak self] (granted: Bool, error:NSError?) -> Void in
                        if granted {
                            self!.insertEvent(eventStore)
                        } else {
                            print("Access denied")
                        }
                    } as! EKEventStoreRequestAccessCompletionHandler)
                default:
                    print("Case Default")
            }
            
            var url : String = "\(Settings().getAPIUrl())?_app=xd&cmd=xperience&eventType=\(ModelManager.sharedInstance.xperienceTitle)&name=\(ModelManager.sharedInstance.xperienceName)&org=\(ModelManager.sharedInstance.xperienceOrg)&email=\(ModelManager.sharedInstance.xperienceEmail)&phone=\(ModelManager.sharedInstance.xperiencePhone)&month=\(month)&day=\(day)&year=\(year)&hour=\(ModelManager.sharedInstance.xperienceHour)&minute=\(ModelManager.sharedInstance.xperienceMinute)&timeofday=\(ModelManager.sharedInstance.xperienceTimeOfDay)"
            url = url.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.url = URL(string: url)
            request.httpMethod = "GET"
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                let alert = UIAlertController(title: "Message Sent SuXessfully.", message: "One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367-6382.", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                }
                
                alert.addAction(okAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func currentSelectedHour(_ currentHour: Int){
        print("Called", terminator: "")
        ModelManager.sharedInstance.xperienceHour = currentHour
        if(currentHour < 10){
            hourField.text = "   0" + String(currentHour)
        }else{
            hourField.text = "   " + String(currentHour)
        }
    }
    
    func currentSelectedMinute(_ currentMinute: Int) {
        ModelManager.sharedInstance.xperienceMinute = currentMinute
        if (currentMinute < 10){
            minuteField.text = "   0" + String(currentMinute)
        }else{
            minuteField.text = "   " + String(currentMinute)
        }
    }
    
    func currentSelectedTimeOfDay(_ currentTimeOfDay: String) {
        if (currentTimeOfDay == "AM") { ModelManager.sharedInstance.xperienceTimeOfDay = 1 }else{ ModelManager.sharedInstance.xperienceTimeOfDay = 2 }
        timeOfDayField.text = "  " + currentTimeOfDay
    }
    
    @IBAction func showHourPicker(_ sender: AnyObject) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            let btn : UIButton = sender as! UIButton
            let popOverViewController: XperienceHourPicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceHourPopOver") as! XperienceHourPicker
            popOverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popOverViewController.preferredContentSize = CGSize(width: 200, height: 150)
            popOverViewController.popoverPresentationController?.sourceRect = CGRect(x: -100, y: 0, width: 200, height: 150)
            popOverViewController.delegate = self
            popOverViewController.popoverPresentationController?.sourceView = btn
            present(popOverViewController, animated: true) { () -> Void in
                print("Completed")
            }
        }
    }

    @IBAction func showMinutePicker(_ sender: AnyObject) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            let btn : UIButton = sender as! UIButton
            let popOverViewController: XperienceMinutePicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceMinutePopOver") as! XperienceMinutePicker
            popOverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popOverViewController.preferredContentSize = CGSize(width: 200, height: 150)
            popOverViewController.popoverPresentationController?.sourceRect = CGRect(x: -100, y: 0, width: 200, height: 150)
            popOverViewController.delegate = self
            popOverViewController.popoverPresentationController?.sourceView = btn
            present(popOverViewController, animated: true) { () -> Void in
                print("Completed")
            }
        }
    }
    
    @IBAction func showTimeOfDayPicker(_ sender: AnyObject) {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            let btn : UIButton = sender as! UIButton
            let popOverViewController: XperienceTimeOfDayPicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceTimeOfDayPopOver") as! XperienceTimeOfDayPicker
            popOverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popOverViewController.preferredContentSize = CGSize(width: 200, height: 150)
            popOverViewController.popoverPresentationController?.sourceRect = CGRect(x: -100, y: 0, width: 200, height: 150)
            popOverViewController.delegate = self
            popOverViewController.popoverPresentationController?.sourceView = btn
            present(popOverViewController, animated: true) { () -> Void in
                print("Completed")
            }
        }
    }
    
    var selectedTextField = UITextField()
    
    func createPicker(_ sender: UITextField){
        // Create toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 186/255, green: 34/255, blue: 42/255, alpha: 1)
        toolBar.sizeToFit()

        switch(selectedTextField.tag){
            case 9:
                // Create buttons
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.donePicker))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.cancelPicker))
                
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                
                let popOverViewController: XperienceHourPicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceHourPopOver") as! XperienceHourPicker
                popOverViewController.delegate = self
                sender.inputView = popOverViewController.view
                sender.inputAccessoryView = toolBar
                break
            
            case 10:
                // Create buttons
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.donePicker))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.cancelPicker))
                
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                
                let popOverViewController: XperienceMinutePicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceMinutePopOver") as! XperienceMinutePicker
                popOverViewController.delegate = self
                sender.inputView = popOverViewController.view
                sender.inputAccessoryView = toolBar
                break
            
            case 11:
                // Create buttons
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.donePicker))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.cancelPicker))
                
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                let popOverViewController: XperienceTimeOfDayPicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceTimeOfDayPopOver") as! XperienceTimeOfDayPicker
                popOverViewController.delegate = self
                sender.inputView = popOverViewController.view
                sender.inputAccessoryView = toolBar
                break
            
            default:
                // Create buttons
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("donePicker2")))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("cancelPicker2")))
                
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                let popOverViewController: XperienceHourPicker = self.storyboard?.instantiateViewController(withIdentifier: "xperienceHourPopOver") as! XperienceHourPicker
                popOverViewController.delegate = self
                sender.inputView = popOverViewController.view
                sender.inputAccessoryView = toolBar
                break
        }
        
        
        // Add pickerview and toolbar to textfield
        
    }
    
    func donePicker() {
        selectedTextField.resignFirstResponder()
    }
    
    func cancelPicker() {
        selectedTextField.resignFirstResponder()
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        print("Deinit called on Xpert")
    }
}

class XperienceNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XperienceViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}

@available(iOS 8.0, *)
class XperienceLeftContainer : UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate, CurrencySelectedDelegate, UITextFieldDelegate{
    
    
    @IBOutlet weak var division: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionLabeliPhone4: UILabel!
    
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var emailAddressField: UITextField!
    @IBOutlet weak var contactNameField: UITextField!
    @IBOutlet weak var orginzationField: UITextField!
    weak var delegate: CurrencySelectedDelegate?
    
    func currencySelected(_ currName: String){
        print("Current Selected \(currName)", terminator: "")
        ModelManager.sharedInstance.xperienceTitle = currName
        division.text = currName
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.orginzationField){
            self.orgChanged(textField)
        }else if (textField == self.emailAddressField){
            self.emailChanged(textField)
        }else if (textField == self.contactNameField){
            self.nameChanged(textField)
        }else if (textField == self.phoneField){
            self.phoneChanged(textField)
        }
        
        return true
    }
    
    @IBAction func nameChanged(_ sender: AnyObject) {
        let field : UITextField = sender as! UITextField
        print("Called Value Changed", terminator: "")
        ModelManager.sharedInstance.xperienceName = field.text!
    }
    
    @IBAction func orgChanged(_ sender: AnyObject){
        let field : UITextField = sender as! UITextField
        ModelManager.sharedInstance.xperienceOrg = field.text!
    }
    
    @IBAction func emailChanged(_ sender: AnyObject){
        let field : UITextField = sender as! UITextField
        ModelManager.sharedInstance.xperienceEmail = field.text!
    }
    
    @IBAction func phoneChanged(_ sender: AnyObject){
        let field : UITextField = sender as! UITextField
        ModelManager.sharedInstance.xperiencePhone = field.text!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    @IBAction func displayPopOver(_ sender: AnyObject) {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            //let senderButton : UIButton = sender as! UIButton
            let popOverViewController: XperienceLeftContainer = self.storyboard?.instantiateViewController(withIdentifier: "xperiencePopOver") as! XperienceLeftContainer
            popOverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popOverViewController.preferredContentSize = CGSize(width: 400, height: 300)
            popOverViewController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: -117, width: 400, height: 300)
            popOverViewController.delegate = self
            popOverViewController.popoverPresentationController?.sourceView = self.view
            present(popOverViewController, animated: true) { () -> Void in
                print("Completed")
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //self.displayPopOver(textField)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
       
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 186/255, green: 34/255, blue: 42/255, alpha: 1)
            toolBar.sizeToFit()
        
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.donePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XperienceViewController.cancelPicker))
        
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
        
            textField.inputAccessoryView = toolBar
            textField.tintColor = UIColor.white
            
            selectedTextField = textField
            
            if(textField.tag == 1){
                self.showKeyboard(textField)
            }
            
            UIView.animate(withDuration: 1.0, animations: {
                self.parent!.view.frame.origin.y -= textField.frame.origin.y
            })
            
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 1.0, animations: {
            self.parent!.view.frame.origin.y += textField.frame.origin.y
        })
        
        return true
    }
    
    var useKeyboard:Bool = false
    @IBOutlet weak var selectedTextField : UITextField!
    
    func showKeyboard(_ textField: UITextField) {
        textField.inputView = nil
        textField.reloadInputViews()
        createPicker(textField)
    }
    
    // That's my custom picker - adjust whatever you need
    func createPicker(_ sender: UITextField){
        selectedTextField = sender
        
        // Create picker view
        var newPickerView: UIPickerView
        newPickerView = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 300))
        newPickerView.backgroundColor = .white
        
        // Only for UIPickerView
        newPickerView.showsSelectionIndicator = true
        newPickerView.delegate = self
        newPickerView.dataSource = self
        
        sender.inputView = newPickerView
    }
    
    func donePicker() {
        selectedTextField.resignFirstResponder()
    }
    
    func cancelPicker() {
        selectedTextField.resignFirstResponder()
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        print("Dimissed")
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath) 
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @IBOutlet weak var pickerView: UIView!
    override func viewDidLoad() {
        //self.pickerView.frame = CGRect(x: 0, y: 153, width: self.pickerView.frame.width, height: 0)
        if (Device() == .iPhone4 || Device() == .iPhone4s){
            //descriptionLabel.hidden = true
            //descriptionLabeliPhone4.hidden = false
        }
        
        if (ModelManager.sharedInstance.isLoggedIn()){
            let modelManager = ModelManager.sharedInstance
            let url : String = "\(Settings().getAPIUrl())?cmd=getbillingshipping&token=\(modelManager.token)"
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.url = URL(string: url)
            request.httpMethod = "GET"
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if data != nil {
                    var allData = JSON(data: data!)
                    print(allData)
                    if let name = allData["bContact"].string {
                        self.contactNameField.text = name
                    }
                    
                    if let name = allData["bCompany"].string {
                        self.orginzationField.text = name
                    }
                    
                    //if let name = ModelManager.sharedInstance.email {
                        self.emailAddressField.text = ModelManager.sharedInstance.email
                    //}
                    
                    if let name = allData["bPhone"].string {
                        self.phoneField.text = name
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    var teamMembers : [String] = ["Formetco Campus Tour",
        "Digital Software Training",
        "Site Evaluation",
        "Conference Call",
        "Digital Artwork Guidance"
    ]
    
    //@IBOutlet weak var teamPicker: UIPickerView!
    
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return teamMembers.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return teamMembers[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*Did select action here.*/
        print("Changed Position", terminator: "")
        delegate?.currencySelected(teamMembers[row])
        if (division != nil){
            division.text = "  " + teamMembers[row]
            ModelManager.sharedInstance.xperienceTitle = teamMembers[row]
        }
    }
}

@available(iOS 8.0, *)
class XperienceRightContainer : UIViewController{
    @IBOutlet weak var CVCalandarMenuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!

    @IBOutlet weak var monthLabel: UILabel!
    var shouldShowDaysOut = true
    var animationFinished = true
    
    override func viewDidLoad() {
        calendarView.calendarDelegate = self
        calendarView.calendarAppearanceDelegate = self
        monthLabel.text = CVDate(date: Foundation.Date()).globalDescription
        ModelManager.sharedInstance.xperienceDate = calendarView.presentedDate.convertedDate()!
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarView.commitCalendarViewUpdate()
        CVCalandarMenuView.commitMenuViewUpdate()
    }
    
    @IBAction func loadPrevious(_ sender: AnyObject) {
        calendarView.loadPreviousView()
    }
    
    
    @IBAction func loadNext(_ sender: AnyObject) {
        calendarView.loadNextView()
    }
}

// MARK: - CVCalendarViewDelegate

@available(iOS 8.0, *)
extension XperienceRightContainer: CVCalendarViewDelegate
{
    func supplementaryView(viewOnDayView dayView: DayView) -> UIView
    {
        let π = M_PI
        
        let ringSpacing: CGFloat = 3.0
        let ringInsetWidth: CGFloat = 1.0
        let ringVerticalOffset: CGFloat = 1.0
        var ringLayer: CAShapeLayer!
        let ringLineWidth: CGFloat = 0.0
        let ringLineColour: UIColor = .blue
        
        let newView = UIView(frame: dayView.bounds)
        
        let diameter: CGFloat = (newView.bounds.width) - ringSpacing
        let radius: CGFloat = diameter / 2.0
        
        let rect = CGRect(x: newView.frame.midX-radius, y: newView.frame.midY-radius-ringVerticalOffset, width: diameter, height: diameter)
        
        ringLayer = CAShapeLayer()
        newView.layer.addSublayer(ringLayer)
        
        ringLayer.fillColor = nil
        ringLayer.lineWidth = ringLineWidth
        ringLayer.strokeColor = ringLineColour.cgColor
        
        let ringLineWidthInset: CGFloat = CGFloat(ringLineWidth/2.0) + ringInsetWidth
        let ringRect: CGRect = rect.insetBy(dx: ringLineWidthInset, dy: ringLineWidthInset)
        let centrePoint: CGPoint = CGPoint(x: ringRect.midX, y: ringRect.midY)
        let startAngle: CGFloat = CGFloat(-π/2.0)
        let endAngle: CGFloat = CGFloat(π * 2.0) + startAngle
        let ringPath: UIBezierPath = UIBezierPath(arcCenter: centrePoint, radius: ringRect.width/2.0, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        ringLayer.path = ringPath.cgPath
        ringLayer.frame = newView.layer.bounds
        
        return newView
    }
    
    func supplementaryView(shouldDisplayOnDayView dayView: DayView) -> Bool
    {
        if (Int(arc4random_uniform(3)) == 1)
        {
            return true
        }
        return false
    }
}


@available(iOS 8.0, *)
extension XperienceRightContainer {
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func firstWeekday() -> Weekday {
        return .sunday
    }
    
    func shouldShowWeekdaysOut() -> Bool {
        return shouldShowDaysOut
    }
    
    func didSelectDayView(_ dayView: CVCalendarDayView) {
        //let date = dayView.date
        ModelManager.sharedInstance.xperienceDate = calendarView.presentedDate.convertedDate()!
        print("\(calendarView.presentedDate.commonDescription) is selected!")
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        if monthLabel.text != date.globalDescription && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            
            UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransform.identity
                
                }) { _ in
                    
                    self.animationFinished = true
                    self.monthLabel.frame = updatedMonthLabel.frame
                    self.monthLabel.text = updatedMonthLabel.text
                    self.monthLabel.transform = CGAffineTransform.identity
                    self.monthLabel.alpha = 1
                    updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
    
    func dotMarker(shouldShowOnDayView dayView: CVCalendarDayView) -> Bool {
        let day = dayView.date.day
        let randomDay = Int(arc4random_uniform(31))
        if day == randomDay {
            return true
        }
        
        return false
    }
    
    @nonobjc func dotMarker(colorOnDayView dayView: CVCalendarDayView) -> UIColor {
        //let day = dayView.date.day
        
        let red = CGFloat(arc4random_uniform(600) / 255)
        let green = CGFloat(arc4random_uniform(600) / 255)
        let blue = CGFloat(arc4random_uniform(600) / 255)
        
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        
        return color
    }
    
    func dotMarker(shouldMoveOnHighlightingOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
}

// MARK: - CVCalendarViewAppearanceDelegate

@available(iOS 8.0, *)
extension XperienceRightContainer: CVCalendarViewAppearanceDelegate {
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return false
    }
    
    func spaceBetweenDayViews() -> CGFloat {
        return 2
    }
}

// MARK: - CVCalendarMenuViewDelegate

@available(iOS 8.0, *)
extension XperienceRightContainer: CVCalendarMenuViewDelegate {
    // firstWeekday() has been already implemented.
}

// MARK: - IB Actions

@available(iOS 8.0, *)
extension XperienceRightContainer {
    @IBAction func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            calendarView.changeDaysOutShowingState(shouldShow: false)
            shouldShowDaysOut = true
        } else {
            calendarView.changeDaysOutShowingState(shouldShow: true)
            shouldShowDaysOut = false
        }
    }
    
    @IBAction func todayMonthView() {
        calendarView.toggleCurrentDayView()
    }
    
    /// Switch to WeekView mode.
    @IBAction func toWeekView(_ sender: AnyObject) {
        calendarView.changeMode(.weekView)
    }
    
    /// Switch to MonthView mode.
    @IBAction func toMonthView(_ sender: AnyObject) {
        calendarView.changeMode(.monthView)
    }
}

// MARK: - Convenience API Demo

@available(iOS 8.0, *)
extension XperienceRightContainer {
    func toggleMonthViewWithMonthOffset(_ offset: Int) {
        let calendar = Calendar.current
        //let calendarManager = calendarView.manager
        
        var components = Manager.componentsForDate(Foundation.Date(), calendar: Calendar.current) // from today
        //components.setValue(<#T##value: Int?##Int?#>, for: Calendar.Component.month)
        //components.month += offset
        
        let resultDate = calendar.date(from: components)!
        
        self.calendarView.toggleViewWithDate(resultDate)
    }
}


@available(iOS 8.0, *)
class XperienceiPhoneViewController : UIViewController, UIAlertViewDelegate {
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
}
