//
//  XpandViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import MessageUI
import AudioToolbox
import AVFoundation

class NonrotatingImagePicker : UIImagePickerController{
    //    override func shouldAutorotate() -> Bool {
    //        return false
    //    }
    
    //    override func supportedInterfaceOrientations() -> Int {
    //        return UIInterfaceOrientation.Portrait.rawValue
    //    }
}

@available(iOS 8.0, *)
class XpandViewController : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate{
    @IBOutlet weak var iPhoneElementsCollectionView: UICollectionView!
    var saved : Bool = false
    var imagePicker : NonrotatingImagePicker = NonrotatingImagePicker()
    var imag : NonrotatingImagePicker = NonrotatingImagePicker()
    @IBOutlet weak var environmentNotification: UIView!
    @IBOutlet weak var environmentImageView: UIImageView!
    @IBOutlet weak var envirnmentOverlayView: UIView!
    var audioPlayer = AVAudioPlayer()
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var shareBtnIcon: UIImageView!
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XpandViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        imagePicker.delegate = self
        self.images = ["xpand_example1", "xpand_example2", "xpand_example3", "xpand_example4", "xpand_example5", "xpand_example6", "xpand_example7", "xpand_example8", "xpand_example9", "xpand_example10", "xpand_example11", "xpand_example12", "xpand_example13", "xpand_example14"]
        self.view.backgroundColor = UIColor(red: 210.0/255.0, green: 211.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            imagesTableView.backgroundColor = UIColor(red: 210.0/255.0, green: 211.0/255.0, blue: 213.0/255.0, alpha: 1.0)
            imagesTableView.reloadData()
        }else{
            iPhoneElementsCollectionView.backgroundColor = UIColor(red: 210.0/255.0, green: 211.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        }
        
        let unfocusGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.onNewTapUnFocus(_:)))
        envirnmentOverlayView.addGestureRecognizer(unfocusGesture)
        
        let dismissTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.onTapDismissNotification(_:)))
        dismissTapGesture.numberOfTapsRequired = 1
        environmentNotification.addGestureRecognizer(dismissTapGesture)
        
        let clearPanGesture : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(XpandViewController.clearEverything(_:)))
        clearPanGesture.minimumNumberOfTouches = 3
        self.envirnmentOverlayView.addGestureRecognizer(clearPanGesture)
        self.shareBtn.backgroundColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
        self.shareBtnIcon.image = UIImage(named: "share_icon_disabled_xpand")
        
        self.moveDown()
    }
    
    @IBAction func goBack(){
        if (saved == false && environmentImageView.image != nil){
            let alert = UIAlertController(title: "X-it without saving?", message: "Please make sure not to leave your masterpiece behind!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Leave", style: UIAlertActionStyle.default, handler: { action in
                self.leaveView = true
                //self.performSegueWithIdentifier("leaveViewSegue", sender: nil)
                self.navigationController?.popToRootViewController(animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Stay", style: UIAlertActionStyle.default, handler: { action in
                self.leaveView = false
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    deinit {
        print("Deinit called on Xpert")
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    func clearEverything(_ sender: AnyObject){
        print("Clear Called", terminator: "")
        self.environmentImageView.image = nil
        //for subview in self.envirnmentOverlayView.subviews{
        self.envirnmentOverlayView.removeAllSubviews()
        //}
        self.envirnmentOverlayView.isHidden = true
        self.environmentImageView.isHidden = true
        self.saved = false
        self.shareBtn.backgroundColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
        self.shareBtnIcon.image = UIImage(named: "share_icon_disabled_xpand")
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    func onTapDismissNotification(_ sender: AnyObject?){
        environmentNotification.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            imagesTableView.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
            imagesTableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            imagesTableView.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
            imagesTableView.reloadData()
        }
    }
    
    var leaveView : Bool = false
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if(saved && identifier == "leaveViewSegue" && environmentImageView.image != nil){
            return true
        }
        
        if(identifier == "leaveViewSegue" && environmentImageView.image != nil){
            if(identifier == "leaveViewSegue" && self.leaveView){
                return self.leaveView
            }
            
            if(identifier == "leaveViewSegue"){
                let alert = UIAlertController(title: "X-it without saving?", message: "Please make sure not to leave your masterpiece behind!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Leave", style: UIAlertActionStyle.default, handler: { action in
                    self.leaveView = true
                    self.performSegue(withIdentifier: "leaveViewSegue", sender: nil)
                }))
                alert.addAction(UIAlertAction(title: "Stay", style: UIAlertActionStyle.default, handler: { action in
                    self.leaveView = false
                }))
                self.present(alert, animated: true, completion: nil)
                return self.leaveView
            }else{
                return true
            }
        }else{
            return true
        }
    }
    
    @IBAction func cameraGalleryClicked(_ sender: AnyObject) {
        
        let senderButton = sender as! UIButton
        if (senderButton.tag == 1){
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imag.delegate = self
                imag.sourceType = UIImagePickerControllerSourceType.camera;
                imag.allowsEditing = false
                
                self.present(imag, animated: true, completion: {
                    NSLog("Completed")
                })
            }
        }else{
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                imag.delegate = self
                imag.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imag.allowsEditing = true
                
                //                self.presentViewController(imag, animated: true, completion: {
                //                    NSLog("Completed")
                //                })
                
                if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                    let popOver : UIPopoverController = UIPopoverController(contentViewController: imag);
                    //let sizeRect = UIScreen.mainScreen().applicationFrame
                    //let width = sizeRect.size.width
                    //let height = sizeRect.size.height
                    popOver.present(from: CGRect(x: senderButton.frame.origin.x, y: senderButton.frame.origin.y, width: 1, height: 1), in: self.view, permittedArrowDirections: UIPopoverArrowDirection.right, animated: true)
                    let delay = 0.2 * Double(NSEC_PER_SEC)
                    let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: time, execute: { () -> Void in
                        popOver .setContentSize(CGSize(width: 500, height: 500), animated: true)
                    })
                } else {
                    self.present(imag, animated: true, completion: {
                        NSLog("Completed")
                    })
                }
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                    imag.delegate = self
                    imag.sourceType = UIImagePickerControllerSourceType.camera;
                    imag.allowsEditing = false
                    
                    self.present(imag, animated: true, completion: {
                        NSLog("Completed")
                    })
                }
            }
        }
        
    }
    
    @IBAction func shareBtnClicked(_ sender: AnyObject) {
        let shareBtn = sender as! UIButton
        if (envirnmentOverlayView.isHidden){
            return
        }
        shareBtn.backgroundColor = UIColor.black
        removeBorderView()
        for view in envirnmentOverlayView.subviews {
            let currentView = view as! UIImageView
            activeImageView = UIImageView()
            
            activeImageView!.frame = CGRect(x: currentView.frame.origin.x, y: currentView.frame.origin.y, width: currentView.frame.size.width, height: currentView.frame.size.height)
            
            activeImageView!.contentMode = UIViewContentMode.scaleAspectFit
            activeImageView!.image = currentView.image
            activeImageView!.isUserInteractionEnabled = true
            
            environmentImageView.addSubview(activeImageView!)
            //view.removeFromSuperview()
        }
        envirnmentOverlayView.isHidden = true
        
        let imageToSend : UIImage = self.generateImage()
        var itemsToShare : [AnyObject] = [AnyObject]()
        itemsToShare.append(imageToSend)
        let shareControllerVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
        shareControllerVC.completionHandler = {(activityType, completed:Bool) in
            if(!completed) { return }
            if (activityType != UIActivityType.saveToCameraRoll){
                UIImageWriteToSavedPhotosAlbum(imageToSend, nil, nil, nil) /*Save the image to photo gallery */
            }
            
            let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "app", ofType: "wav")!)
            print(alertSound)
            
            do {
                // Removed deprecated use of AVAudioSessionDelegate protocol
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch _ {
            }
            do {
                try AVAudioSession.sharedInstance().setActive(true)
            } catch _ {
            }
            
            var error:NSError?
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: alertSound)
            } catch let error1 as NSError {
                error = error1
                self.audioPlayer = AVAudioPlayer()
            } catch {
                fatalError()
            }
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
            
            self.environmentNotification.isHidden = false
            self.environmentNotification.alpha = 0
            UIView.animate(withDuration: 1, animations: {
                self.environmentNotification.alpha = 1
            })
            let seconds = 4.0
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
            
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                UIView.animate(withDuration: 1, animations: {
                    self.environmentNotification.alpha = 0
                })
                
                let seconds = 1.0
                let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
                let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
                
                DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                    self.environmentNotification.isHidden = true
                })
            })
            
            self.saved = true
        }
        
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            shareControllerVC.popoverPresentationController?.sourceView = self.view
            let frame : CGRect = CGRect(x: shareBtn.frame.origin.x, y: shareBtn.frame.origin.y + 20, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height / 2) )
            shareControllerVC.popoverPresentationController?.sourceRect = frame
        }
        
        self.present(shareControllerVC, animated: true, completion: {() in
            self.shareBtn.backgroundColor = UIColor(red: 155/255.0, green: 0/255.0, blue: 32/255.0, alpha: 1.0)
        })
        
        
        for view in environmentImageView.subviews{
            view.removeFromSuperview()
        }
        envirnmentOverlayView.isHidden = false
    }
    
    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
        let imageRef = image.cgImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        context!.interpolationQuality = CGInterpolationQuality.high
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
        
        context?.concatenate(flipVertical)
        // Draw into the context; this scales the image
        context?.draw(imageRef!, in: newRect)
        
        let newImageRef = context?.makeImage()
        let newImage = UIImage(cgImage: newImageRef!)
        
        // Get the resized image from the context and a UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func generateImage() -> UIImage{
        UIGraphicsBeginImageContextWithOptions(environmentImageView.frame.size, false, 0)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        environmentImageView.layer.render(in: context)
        let img : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return img
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        autoreleasepool {
            NSLog("Image Picked")
            environmentImageView.contentMode = UIViewContentMode.scaleToFill
            environmentImageView.image = image
            environmentImageView.isHidden = false
            envirnmentOverlayView.isHidden = false
            imag.dismiss(animated: true, completion: nil)
            self.shareBtn.backgroundColor = UIColor(red: 152/255.0, green: 0/255.0, blue: 32/255.0, alpha: 1.0)
            self.shareBtnIcon.image = UIImage(named: "ShareIcon")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        NSLog("Dismissing")
        imag.dismiss(animated: true, completion: nil)
    }
    
    /*You will need to create your container then connect them here.*/
    @IBOutlet weak var imagesTableView: UITableView!
    @IBOutlet weak var imagesContainer:UIView?
    @IBOutlet weak var imageBarScrollView:UIScrollView?
    /*End Configure of segues here!*/
    
    var imageIndex = 0 /*Actively used image index*/
    var images : [String] = [] /*Will be initilazed when loaded, you would call dynamic call then for loading images*/
    
    var defaultWidth = 200 /*Default image width*/
    var defaultHeight = 125 /*Default image height*/
    var defaultX = 25 /*Default origin X for first image*/
    var defaultY = 1 /*Default origin Y for first image*/
    var horizontal = false /*Default layout view, landscape mode*/
    var vertical = true /*Change to true if you want it in portrait mode*/
    var defaultSpacingX = 70 /*The amount in between each drag image. (Only used if horizontal)*/
    var defaultSpacingY = 150 /*The amount in between each drag image. (Only used if vertical)*/
    var defaultScrollContainerHeight = 70 /*This only applies in horizontal mode*/
    var defaultScrollContainerWidth = 200 /*This only applies in vertical mode*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.images.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        imageIndex = indexPath.row
        
        let cell : XpandDraggableCell = tableView.dequeueReusableCell(withIdentifier: "XpandDraggableCell", for: indexPath) as! XpandDraggableCell
        cell.backgroundColor = UIColor(red: 210.0/255.0, green: 211.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        cell.imgview.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        
        if (horizontal) {
            defaultX = defaultSpacingX * imageIndex; /* E.g 70 * 2 = 140 from origin X. */
        }else{
            defaultY = defaultSpacingY * imageIndex; /* E.g 70 * 2 = 140 from origin Y. */
        }
        
        //Debug mode
        //cell.imgview.layer.borderColor = UIColor.redColor().CGColor
        //cell.imgview.layer.borderWidth = 1
        
        cell.imgview.frame = CGRect(x: cell.imgview.frame.origin.x, y: cell.imgview.frame.origin.y, width: 75.0, height: 75.0)
        let cellWidth : CGFloat = cell.contentView.frame.width
        let cellHeight : CGFloat = cell.contentView.frame.height
        cell.imgview.center = CGPoint(x: cell.contentView.frame.origin.x + (cellWidth / 2), y: cell.contentView.frame.origin.y + (cellHeight / 2))
        autoreleasepool {
            cell.imgview.image = UIImage(named: images[imageIndex]) /*Image provided*/
        }
        cell.imgview.contentMode = UIViewContentMode.scaleAspectFit
        cell.imgview.isUserInteractionEnabled = true
        cell.imgview.tag = imageIndex
        
        let addToViewPanGesture : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(XpandViewController.imageViewPanned(_:)))
        addToViewPanGesture.delegate = self
        cell.imgview.addGestureRecognizer(addToViewPanGesture)
        
        let doubleTapAddGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.cellDoubleTapped(_:)))
        doubleTapAddGesture.numberOfTapsRequired = 2
        cell.imgview.addGestureRecognizer(doubleTapAddGesture)
        
        cell.imgview.backgroundColor = UIColor.clear
        cell.imgview.layer.backgroundColor = UIColor.clear.cgColor
        cell.imgviewbtn.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        
        return cell
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture : UIPanGestureRecognizer = gestureRecognizer as! UIPanGestureRecognizer
        let gestureView : UIView = gestureRecognizer.view!
        let translation : CGPoint = gesture.translation(in: gestureView.superview!)
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            if(fabsf(Float(translation.x)) > fabsf(Float(translation.y))){
                print("Horizontal Pan Gesture", terminator: "")
                return true
            }
            print("Vertical Pan Gesture", terminator: "")
            return false
        }else{
            if(!(fabsf(Float(translation.x)) > fabsf(Float(translation.y)))){
                print("Vertical Pan Gesture", terminator: "")
                return true
            }
            print("Horizontal Pan Gesture", terminator: "")
            return false
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        imageIndex = indexPath.row
        //tableView.dequeueReusableCellWithIdentifier("XpandDraggableCell", forIndexPath: indexPath) as! XpandDraggableCell
        let cell : XpandDraggableCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "XpandDraggableCell", for: indexPath) as! XpandDraggableCollectionCell
        cell.backgroundColor = UIColor(red: 210.0/255.0, green: 211.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        cell.imgview.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        
        if (horizontal) {
            defaultX = defaultSpacingX * imageIndex; /* E.g 70 * 2 = 140 from origin X. */
        }else{
            defaultY = defaultSpacingY * imageIndex; /* E.g 70 * 2 = 140 from origin Y. */
        }
        
        //Debug mode
        //cell.imgview.layer.borderColor = UIColor.redColor().CGColor
        //cell.imgview.layer.borderWidth = 1
        
        cell.imgview.frame = CGRect(x: cell.imgview.frame.origin.x, y: cell.imgview.frame.origin.y, width: 125.0, height: 75.0)
        let cellWidth : CGFloat = cell.contentView.frame.width
        let cellHeight : CGFloat = cell.contentView.frame.height
        cell.imgview.center = CGPoint(x: cell.contentView.frame.origin.x + (cellWidth / 2), y: cell.contentView.frame.origin.y + (cellHeight / 2))
        autoreleasepool {
            cell.imgview.image = UIImage(named: images[imageIndex]) /*Image provided*/
        }
        cell.imgview.contentMode = UIViewContentMode.scaleAspectFit
        cell.imgview.isUserInteractionEnabled = true
        cell.imgview.tag = imageIndex
        
        let addToViewPanGesture : UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(XpandViewController.imageViewPanned(_:)))
        addToViewPanGesture.delegate = self
        cell.imgview.addGestureRecognizer(addToViewPanGesture)
        
        let doubleTapAddGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.cellDoubleTapped(_:)))
        doubleTapAddGesture.numberOfTapsRequired = 2
        cell.imgview.addGestureRecognizer(doubleTapAddGesture)
        
        cell.imgview.backgroundColor = UIColor.clear
        cell.imgview.layer.backgroundColor = UIColor.clear.cgColor
        cell.imgviewbtn.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor(red: 210/255.0, green: 211/255.0, blue: 213/255.0, alpha: 1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func imageViewTouched(_ touchGestureRecognizer: UITapGestureRecognizer){
        let imageView = touchGestureRecognizer.view as! UIImageView
        currentSelectedImageView = imageView
    }
    
    func cellDoubleTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        let clickedImageView = tapGestureRecognizer.view as! UIImageView
        if (!envirnmentOverlayView.isHidden && !environmentImageView.isHidden){
            if (tapGestureRecognizer.state == UIGestureRecognizerState.ended) {
                removeBorderView()
                activeImageView = UIImageView()
                
                activeImageView!.frame = CGRect(x: 0, y: 0, width: clickedImageView.frame.size.width, height: clickedImageView.frame.size.height)
                
                activeImageView!.contentMode = UIViewContentMode.scaleAspectFit
                autoreleasepool {
                    activeImageView!.image = clickedImageView.image
                }
                activeImageView!.isUserInteractionEnabled = true
                
                let newPanGesture = UIPanGestureRecognizer(target: self, action: #selector(XpandViewController.onNewPan(_:)))
                activeImageView!.addGestureRecognizer(newPanGesture)
                
                let newPinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(XpandViewController.onNewPinch(_:)))
                activeImageView!.addGestureRecognizer(newPinchGesture)
                
                let newRotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(XpandViewController.onNewRotation(_:)))
                activeImageView!.addGestureRecognizer(newRotationGesture)
                
                let newTapGesture = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.onNewTap(_:)))
                newTapGesture.numberOfTapsRequired = 2 /*delete view*/
                activeImageView!.addGestureRecognizer(newTapGesture)
                
                envirnmentOverlayView.addSubview(activeImageView!)
                addBorderView()
            }
        }
    }
    
    var currentSelectedImageView : UIImageView?
    
    func imageViewPanned(_ panGestureRecognizer: UIPanGestureRecognizer){
        if (!envirnmentOverlayView.isHidden && !environmentImageView.isHidden){
            let clickedImageView = panGestureRecognizer.view as! UIImageView
            let location = panGestureRecognizer.location(in: view)
            
            if (panGestureRecognizer.state == UIGestureRecognizerState.began) {
                print("Active image")
                
                activeImageView = UIImageView()
                
                activeImageView!.frame = CGRect(x: clickedImageView.frame.origin.x,y: clickedImageView.frame.origin.y, width: clickedImageView.frame.size.width, height: clickedImageView.frame.size.height)
                
                activeImageView!.contentMode = UIViewContentMode.scaleAspectFit
                autoreleasepool {
                    activeImageView!.image = clickedImageView.image
                }
                activeImageView!.isUserInteractionEnabled = true
                
                
                let newPanGesture = UIPanGestureRecognizer(target: self, action: #selector(XpandViewController.onNewPan(_:)))
                activeImageView!.addGestureRecognizer(newPanGesture)
                
                let newPinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(XpandViewController.onNewPinch(_:)))
                activeImageView!.addGestureRecognizer(newPinchGesture)
                
                let newRotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(XpandViewController.onNewRotation(_:)))
                activeImageView!.addGestureRecognizer(newRotationGesture)
                
                let newTapGesture = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.onNewTap(_:)))
                newTapGesture.numberOfTapsRequired = 2 /*delete view*/
                activeImageView!.addGestureRecognizer(newTapGesture)
                
                let newTapGestureActive = UITapGestureRecognizer(target: self, action: #selector(XpandViewController.onNewTapActive(_:)))
                newTapGestureActive.numberOfTapsRequired = 1 /*delete view*/
                activeImageView!.addGestureRecognizer(newTapGestureActive)
                
                envirnmentOverlayView.addSubview(activeImageView!)
                addBorderView()
                
            } else if (panGestureRecognizer.state == UIGestureRecognizerState.changed) {
                removeBorderView()
                activeImageView!.center = location
                addBorderView()
            }
        }
    }
    
    var activeImageView : UIImageView?
    
    func onNewTap(_ tapGestureRecognizer: UITapGestureRecognizer){
        removeBorderView()
        let imageView = tapGestureRecognizer.view as! UIImageView
        imageView.isHidden = true
        imageView.removeFromSuperview()
    }
    
    func onNewTapActive(_ tapGestureRecognizer: UITapGestureRecognizer){
        removeBorderView()
        let imageView = tapGestureRecognizer.view as! UIImageView
        self.activeImageView = imageView
        addBorderView()
    }
    
    func onNewTapUnFocus(_ tapGestureRecognizer: UITapGestureRecognizer){
        removeBorderView()
    }
    
    var borderImageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var borderLeftMarqueeImageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var borderRightMarqueeImageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var borderTopMarqueeImageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var borderBottomMarqueeImageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    func removeBorderView(){
        for view in envirnmentOverlayView.subviews{
            if (view.tag > 998){
                view.removeFromSuperview()
            }
        }
    }
    
    func addBorderView(){
        let borderFrame : CGRect = CGRect(x: self.activeImageView!.frame.origin.x, y: self.activeImageView!.frame.origin.y, width: self.activeImageView!.frame.width + 1, height: self.activeImageView!.frame.height + 1)
        self.borderImageView = UIImageView(frame: borderFrame)
        borderImageView.layer.borderColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.5).cgColor
        borderImageView.layer.borderWidth = 3
        borderImageView.tag = 999 /*Border imageview tag*/
        envirnmentOverlayView.addSubview(borderImageView)
        
        let leftMarqueeToolFrame : CGRect = CGRect(x: borderFrame.origin.x - 3, y: (borderFrame.origin.y + (borderFrame.height / 2)), width: 10, height: 10)
        let rightMarqueeToolFrame : CGRect = CGRect(x: (borderFrame.origin.x + borderFrame.width - 5), y: (borderFrame.origin.y + (borderFrame.height / 2)), width: 10, height: 10)
        let topMarqueeToolFrame : CGRect = CGRect(x: (borderFrame.origin.x + (borderFrame.width / 2)), y: borderFrame.origin.y - 3 , width: 10, height: 10)
        let bottomMarqueeToolFrame : CGRect = CGRect(x: (borderFrame.origin.x + (borderFrame.width / 2)), y: (borderFrame.origin.y + borderFrame.height - 5), width: 10, height: 10)
        
        self.borderLeftMarqueeImageView = UIImageView(frame: leftMarqueeToolFrame)
        self.borderRightMarqueeImageView = UIImageView(frame: rightMarqueeToolFrame)
        self.borderTopMarqueeImageView = UIImageView(frame: topMarqueeToolFrame)
        self.borderBottomMarqueeImageView = UIImageView(frame: bottomMarqueeToolFrame)
        self.borderLeftMarqueeImageView.backgroundColor = UIColor.white
        self.borderRightMarqueeImageView.backgroundColor = UIColor.white
        self.borderTopMarqueeImageView.backgroundColor = UIColor.white
        self.borderBottomMarqueeImageView.backgroundColor = UIColor.white
        self.borderLeftMarqueeImageView.tag = 1000
        self.borderRightMarqueeImageView.tag = 1001
        self.borderTopMarqueeImageView.tag = 1002
        self.borderBottomMarqueeImageView.tag = 1003
        
        //var scaleMarqueeGesture : UIPanGestureRecognizer = UIPanGestureRecognizer(target: nil, action: Selector("onScaleGestureHorizontal:"))
        
        //self.borderLeftMarqueeImageView.addGestureRecognizer(scaleMarqueeGesture)
        //self.borderRightMarqueeImageView.addGestureRecognizer(scaleMarqueeGesture)
        //self.borderTopMarqueeImageView.addGestureRecognizer(scaleMarqueeGesture)
        //self.borderBottomMarqueeImageView.addGestureRecognizer(scaleMarqueeGesture)
        
        envirnmentOverlayView.addSubview(borderLeftMarqueeImageView)
        envirnmentOverlayView.addSubview(borderRightMarqueeImageView)
        envirnmentOverlayView.addSubview(borderTopMarqueeImageView)
        envirnmentOverlayView.addSubview(borderBottomMarqueeImageView)
    }
    
    var currentLocationHorizontalMarquee : CGPoint = CGPoint(x: 0, y: 0)
    
    func onScaleGestureHorizontal(_ panGestureRecognizer: UIPanGestureRecognizer){
        let currentImageView : UIImageView = panGestureRecognizer.view as! UIImageView
        if (panGestureRecognizer.state == UIGestureRecognizerState.began){
            currentLocationHorizontalMarquee = CGPoint(x: currentImageView.frame.origin.x, y: currentImageView.frame.origin.y)
        }else if (panGestureRecognizer.state == UIGestureRecognizerState.changed){
            //let newX : CGFloat = currentImageView.frame.origin.x
            //var difX : CGFloat = newX - currentLocationHorizontalMarquee.x
        }else if (panGestureRecognizer.state == UIGestureRecognizerState.ended){
            
        }
    }
    
    func onNewPan(_ panGestureRecognizer: UIPanGestureRecognizer) {
        self.activeImageView = panGestureRecognizer.view as? UIImageView
        
        let clickedImageView = panGestureRecognizer.view as! UIImageView
        let location = panGestureRecognizer.location(in: view)
        print("Active Image")
        
        if (panGestureRecognizer.state == UIGestureRecognizerState.began) {
            removeBorderView()
            
            self.activeImageView!.frame = CGRect(x: clickedImageView.frame.origin.x,y: clickedImageView.frame.origin.y, width: clickedImageView.frame.size.width, height: clickedImageView.frame.size.height)
            
            addBorderView()
            
        } else if (panGestureRecognizer.state == UIGestureRecognizerState.changed) {
            UIView.animate(withDuration: 0.1, animations: {
                self.removeBorderView()
                self.activeImageView!.center = location
                self.addBorderView()
            })
        }
    }
    
    func onNewPinch(_ pinchGestureRecognizer: UIPinchGestureRecognizer) {
        removeBorderView()
        let t : CGAffineTransform = self.activeImageView!.transform
        let xscale : CGFloat = sqrt(t.a * t.a + t.c * t.c)
        let yscale : CGFloat = sqrt(t.b * t.b + t.d * t.d)
        
        UIView.animate(withDuration: 0.1, animations: {
            self.activeImageView!.transform = CGAffineTransform(scaleX: pinchGestureRecognizer.scale, y: pinchGestureRecognizer.scale)
        })
        
        print("Current X-Scale: \(xscale) , Current Y-Scale: \(yscale) , New Scale: \(pinchGestureRecognizer.scale)")
        addBorderView()
    }
    
    func onNewRotation(_ rotationGestureRecognizer: UIRotationGestureRecognizer) {
        //Bug with rotation
        //activeImageView!.transform = CGAffineTransformMakeRotation(rotationGestureRecognizer.rotation)
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
}

class XpandNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XpandViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}

class XpandTableViewController : UITableView{
    
}

class XpandDraggableCell : UITableViewCell{
    @IBOutlet weak var imgviewbtn: UIButton!
    @IBOutlet weak var imgview: UIImageView!
}

class XpandDraggableCollectionCell : UICollectionViewCell{
    @IBOutlet weak var imgviewbtn: UIButton!
    @IBOutlet weak var imgview: UIImageView!
}
