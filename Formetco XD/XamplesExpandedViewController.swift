//
//  XamplesExpandedViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class XamplesExpandedViewController : UIViewController{
    @IBOutlet weak var xampleTypeLabel: UILabel!
    
    static var xampleTypeID : Int = 0
    static var xample : Xample!
    
    @IBOutlet weak var errorContainerView: UIView!
    override func viewDidLoad() {
        XamplesExpandedViewController.xample = ModelManager.sharedInstance.determineXample(XamplesExpandedViewController.xampleTypeID)
        xampleTypeLabel.text = XamplesExpandedViewController.xample.properName
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            if(Reachability.isConnectedToNetwork() == false){
                errorContainerView.isHidden = false
            }
        }
    }
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("Call Pressed")
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    @IBAction func goBackHome(_ sender: AnyObject) {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            let xamplesViewController = self.storyboard?.instantiateViewController(withIdentifier: "xamplesnavview") as! XamplesNavigationController
        
            self.navigationController?.present(xamplesViewController, animated: true, completion: nil)
        }else{
            self.forceRotateToPortrait()
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = ModelManager.storyboard
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "xamplesnavview") as! UINavigationController
            appdelegate.window!.rootViewController = nav
            self.forceRotateToPortrait()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape
    }
    
    deinit {
        print("deinit called")
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            self.forceRotateToPortrait()
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
}

class XamplesExpandedNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XamplesExpandedNavigationController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        self.forceRotateToLandscapeRight()
        //print(self.navigationController?.viewControllers[0])
    }

    func gotoHome(){
        self.forceRotateToPortrait()
        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
            
        self.navigationController?.present(homeViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    deinit {
        print("deinit called")
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            self.forceRotateToPortrait()
        }
    }
}

@available(iOS 8.0, *)
class XamplesExpandedCollectionViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet weak var xamplesCollectionView: UICollectionView!

    var links : [String] = []
    
    override func viewDidLoad() {
        xamplesCollectionView.layoutMargins = UIEdgeInsets.zero
        xamplesCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let xample : Xample = XamplesExpandedViewController.xample
        let cell : XamplesExpandedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "xamplesCell", for: indexPath) as! XamplesExpandedCell
        
        cell.xampleFolder = xample.folderName
        cell.xampleCellID = indexPath.row
        
        let link = self.links[indexPath.row]
        print(link)
        
        ImageLoader.sharedLoader.imageForUrl(link, completionHandler: {(image: UIImage?, url: String) in
            autoreleasepool {
                if let img : UIImage = image {
                    cell.xampleImageView.image = img
                }else{
                    print("Invalid image found.")
                }
            }
        })
        
        
        //cell.layoutMargins = UIEdgeInsetsZero
        cell.layoutMargins.right = 0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let currentXample : Xample = XamplesExpandedViewController.xample
        let lowercaseTitle = currentXample.properName.lowercased()
        for xample in ModelManager.sharedInstance.xamples {
            let xampleTitle = xample[0].stringValue.lowercased()
            if ( xampleTitle == lowercaseTitle ) {
                let xampleLinks : [JSON] = xample[1].arrayValue
                for xampleLink in xampleLinks {
                    var xLink = xampleLink["link"]
                    self.links.append(xLink.stringValue)
                }
            }
        }
        
        return self.links.count
    }
}

class XamplesExpandedCell : UICollectionViewCell{
    @IBOutlet weak var xampleImageView: UIImageView!
    var xampleFolder : String!
    var xampleCellID : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
