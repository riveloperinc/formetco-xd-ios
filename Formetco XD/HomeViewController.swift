//
//  HomeViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class HomeViewController : UIViewController, SideBarDelegate, UIAlertViewDelegate{
    override func viewDidLoad() {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            self.forceRotateToPortrait()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView;
        self.prepareSidebar()
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (opened){
            self.leftSideNavClicked(animated as AnyObject)
        }
        
        super.viewWillDisappear(animated)
        if(self.isMovingFromParentViewController || self.isBeingDismissed){
            self.dispose()
        }
    }
    
    deinit {
        self.dispose()
    }
    
    @IBOutlet weak var aboutView: UIView!
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    
    @IBAction func tileClicked(_ sender: AnyObject) {
        print("Clicked", terminator: "")
    }
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Confirm", message: "You are about to navigate out of this app to http://www.formetco.com/ftx", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { action in
            let filename : String = "http://www.formetco.com/ftx"
            let link = filename.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            UIApplication.shared.openURL(URL(string: String(describing: link))!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
        
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
        
                alert.addAction(okAction)
                alert.addAction(cancelAction)
        
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    var sideBar:SideBar = SideBar()
    
    func prepareSidebar(){
        var items : Array<String> = []
        if(ModelManager.sharedInstance.isLoggedIn()){
            items = ["Home", "Settings", "About", "Logout"]
        }else{
            items = ["Home", "Login", "About"]
        }
        sideBar = SideBar(sourceView: self.view, menuItems: items)
        sideBar.delegate = self
    }
    
    var opened : Bool = false
    
    @IBAction func leftSideNavClicked(_ sender: AnyObject) {
        if !opened{
            opened = true
            sideBar.showSideBar(opened)
        }else{
            opened = false
            if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                self.aboutView.isHidden = true
            }
            sideBar.showSideBar(opened)
        }
    }
    
    func sideBarDidSelectButtonAtIndex(_ index: Int) {
        if(!ModelManager.sharedInstance.isLoggedIn()){
            switch (index){
                case 0:
                    if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                        self.aboutView.isHidden = true
                    }
                    gotoHome()
                    break
            
                case 1:
                    if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                        self.aboutView.isHidden = true
                    }
                    ModelManager.sharedInstance.fromHome = true
                    gotoLogin()
                    break
            
                case 2:
                    gotoAbout()
                    break
            
                default:
                    break
            }
        }else{
            switch(index){
                case 0:
                    if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                        self.aboutView.isHidden = true
                    }
                    gotoHome()
                    break
                
                case 1:
                    if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                        self.aboutView.isHidden = true
                    }
                    gotoSettings()
                    break
                
                case 2:
                    gotoAbout()
                    break
                
                case 3:
                    if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
                        self.aboutView.isHidden = true
                    }
                    ModelManager.sharedInstance.email = ""
                    ModelManager.sharedInstance.token = ""
                    gotoHome()
                    break
                
                default:
                    break
            }
        }
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    func gotoLogin(){
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginview") as! LoginViewController
        let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(HomeViewController.dismissView(_:)))
        loginViewController.navigationItem.leftBarButtonItem = navigationControllerExitBtn
        self.navigationController?.pushViewController(loginViewController, animated: true)
        self.dispose()
    }
    
    func dismissView(_ sender: AnyObject?){
        gotoHome()
    }
    
    func gotoAbout(){
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            self.aboutView.isHidden = false
        }else{
            let aboutNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "aboutnavview") as! AboutNavigationController
            
            self.navigationController?.present(aboutNavViewController, animated: true, completion: nil)
            self.dispose()
        }
    }
    
    func gotoSettings(){
        let settingsViewController = self.storyboard?.instantiateViewController(withIdentifier: "settingsview") as! AccountSettingViewController
        
        self.navigationController?.pushViewController(settingsViewController, animated: true)
        self.dispose()
    }
    
    func sideBarWillClose() {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            self.aboutView.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
}

class HomeNavigationController : UINavigationController{
    override func viewDidLoad() {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            self.forceRotateToPortrait()
            self.moveDown()
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
            return UIInterfaceOrientation.Portrait
        }else{
            return UIInterfaceOrientation.LandscapeRight
        }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(self.isMovingFromParentViewController || self.isBeingDismissed){
            self.dispose()
        }
    }
    
    /*override func popViewControllerAnimated(animated: Bool) -> UIViewController? {
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        return super.popViewControllerAnimated(animated)
    }*/
    
    deinit {
        self.dispose()
    }
}

@available(iOS 8.0, *)
class HomeTableViewController : UITableViewController, UIAlertViewDelegate{
    
    var leaveAlertView = UIAlertView()
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        let device : UIDevice = UIDevice.current
        let systemVersion = (device.systemVersion as NSString).floatValue
        if(systemVersion < 8.0) {
            let alert = leaveAlertView
            alert.title = "Confirm"
            alert.message = "You are about to navigate out of this app to http://www.formetco.com/ftx"
            alert.addButton(withTitle: "Confirm")
            alert.addButton(withTitle: "Cancel")
            alert.show()
            alert.delegate = self
        }else{
            let alert = UIAlertController(title: "Confirm", message: "You are about to navigate out of this app to http://www.formetco.com/ftx", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { action in
                let filename : String = "http://www.formetco.com/ftx"
                let link = filename.addingPercentEscapes(using: String.Encoding.utf8)! as String!
                //UIApplication.shared.openURL(URL(string: String(describing: link))!)
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: link!)!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(URL(string: link!)!)
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == leaveAlertView){
            if (buttonIndex == 0){
                let filename : String = "http://www.formetco.com/ftx"
                let link = filename.addingPercentEscapes(using: String.Encoding.utf8)! as String!
                UIApplication.shared.openURL(URL(string: String(describing: link))!)
            }
        }
    }
    
    @IBOutlet weak var xpandTile: UIButton!
    override func viewDidLoad() {
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
}
