//
//  XpertViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class XpertViewController : UIViewController, UITextFieldDelegate, UITextViewDelegate{
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var orgField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var questionsField: UITextView!
    
    var starterFrame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    override func viewDidLoad() {
        if submitBtn != nil{
            submitBtn.layer.borderColor = UIColor.white.cgColor
            submitBtn.layer.borderWidth = 1
        }
        
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone && callBtn != nil){
            callBtn.layer.borderColor = UIColor.white.cgColor
            callBtn.layer.borderWidth = 1
        }
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XpertViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        NotificationCenter.default.addObserver(self, selector: #selector(XpertViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(XpertViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        starterFrame = self.view.frame

        self.moveDown()
        
        if (ModelManager.sharedInstance.isLoggedIn()){
            let modelManager = ModelManager.sharedInstance
            let url : String = "\(Settings().getAPIUrl())?cmd=getbillingshipping&token=\(modelManager.token)"
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.url = URL(string: url)
            request.httpMethod = "GET"
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if data != nil {
                    var allData = JSON(data: data!)
                    if let name = allData["bContact"].string {
                        if let textField = self.nameField{
                            textField.text = name
                        }
                    }
                    if let org = allData["bCompany"].string {
                        if let textField = self.orgField{
                            textField.text = org
                        }
                    }
                    if let textField = self.emailField{
                        textField.text = ModelManager.sharedInstance.email
                    }
                    if let phone = allData["bPhone"].string {
                        if let textField = self.phoneField{
                            textField.text = phone
                        }
                    }
                }
            }
        }
    }
    
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    var selectedTextField = UITextField()
    var _active = false
    var selectedTextView = UITextView()
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.selectedTextField = textField
        print("Animated started", terminator: "")
        
        self._active = true
        
        // Create toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 186/255, green: 34/255, blue: 42/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Create buttons
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XpertViewController.doneToolbar))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XpertViewController.cancelToolbar))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        
        
        return true
    }
    
    func doneToolbar (){
        selectedTextField.resignFirstResponder()
    }
    
    func cancelToolbar (){
        selectedTextField.resignFirstResponder()
    }
    
    func doneToolbarTextView (){
        selectedTextView.resignFirstResponder()
    }
    
    func cancelToolbarTextView (){
        selectedTextView.resignFirstResponder()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //if (self.__active){
            //UIView.animateWithDuration(1.0, animations: {
            //    self.view.frame.origin.y += textField.frame.origin.y
            //})
        //}
        //self._active = false
        
        return true
    }
    
    //var __active = false
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //self.__active = true
        //if (!self._active){
            //UIView.animateWithDuration(1.0, animations: {
            //    self.view.frame.origin.y -= 250
            //})
        //}
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.frame.origin.y = textView.frame.origin.y
        })
        
        self.selectedTextView = textView
        //print("Animated started")
        /*UIView.animateWithDuration(1.0, animations: {
            self.view.frame.origin.y -= textView.frame.origin.y
        }*/
        
        // Create toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 186/255, green: 34/255, blue: 42/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Create buttons
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XpertViewController.doneToolbarTextView))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(XpertViewController.cancelToolbarTextView))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textView.inputAccessoryView = toolBar
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.view.frame.origin.y = 0
        return true
    }
    
    func keyboardWillShow(_ sender: Notification) {

    }
    
    func keyboardWillHide(_ sender: Notification) {

    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        print("Deinit called on Xpert")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    @IBAction func submitBtnClicked(_ sender: AnyObject) {
        let name : String = nameField.text!
        let org : String = orgField.text!
        let phone : String = phoneField.text!
        let email : String = emailField.text!
        let questions : String = questionsField.text
        
        
        if (name == "" || org == "" || phone == "" || email == "" || questions == ""){
            let alert = UIAlertController(title: "Whoops!", message: "Please check the form above, and try again!", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                UIAlertAction in
               
            }
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
        }else{
            /*Submit*/
            var url : String = "\(Settings().getAPIUrl())?_app=xd&cmd=xpert&name=\(name)&org=\(org)&phone=\(phone)&email=\(email)&questions=\(questions)"
            url = url.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            print(url, terminator: "")
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.url = URL(string: url)
            request.httpMethod = "GET"
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                print(error!, terminator: "")
                //if data != nil {
                    let alert = UIAlertController(title: "Message Sent SuXessfully.", message: "One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367 6382.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                //}
            }
        }
    }
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("Call Pressed")
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    @IBAction func callSupport(_ sender: AnyObject) {
        if UIDevice.current.userInterfaceIdiom != .pad{
            let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("Call Pressed")
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
}

class XpertNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XpertViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
}
