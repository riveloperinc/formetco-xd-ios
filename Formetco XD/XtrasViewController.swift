//
//  XtrasViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class XtrasViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate{
    @IBOutlet weak var xtrasLabel: UILabel!
    @IBOutlet weak var errorContainerView: UIView!
    @IBOutlet weak var xtraCollectionView: UICollectionView!
    var defaultHeight : CGFloat = 0
    var defaultY : CGFloat = 0
    
    var defaultHeightLabel : CGFloat = 0
    var defaultYLabel : CGFloat = 0
    
    var unloaded : Bool = false
    override func viewDidLoad() {
        //self.xtraCollectionView.reloadData()
        if (Reachability.isConnectedToNetwork() == false){
            errorContainerView.isHidden = false
        }
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            self.defaultHeight = self.xtraCollectionView.frame.height
            self.defaultY = self.xtraCollectionView.frame.origin.y
            self.defaultHeightLabel = self.xtrasLabel.frame.height
            self.defaultYLabel = self.xtrasLabel.frame.origin.y
        }
        
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XtrasViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        self.moveDown()
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let v : UICollectionView = self.xtraCollectionView{
            v.delegate = nil
        }
        
    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        print("Deinit called on xtras", terminator: "")
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.xtraCollectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.xtraCollectionView.reloadData()
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    var previousY : CGFloat = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let amountScrolled : CGFloat = scrollView.contentOffset.y + 10
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            if ((self.defaultHeight + amountScrolled) < UIScreen.main.bounds.height){
                self.xtrasLabel.frame = CGRect(x: self.xtrasLabel.frame.origin.x, y: self.defaultYLabel - amountScrolled, width: self.xtrasLabel.frame.width, height: self.defaultHeightLabel)
            
                self.xtraCollectionView.frame = CGRect(x: self.xtraCollectionView.frame.origin.x, y: self.defaultY - amountScrolled, width: self.xtraCollectionView.frame.width, height: self.defaultHeight + amountScrolled)
            }
        }
    }
    
    var xtras : [Xtra] = ModelManager.sharedInstance.xtras/*[
        Xtra(fileName: "Digital-Billboards-Deliver-Critical-Information", properName: "Digital Billboards Deliver Critical Information", locked: false),
        Xtra(fileName: "Factors-affecting-electrical-costs", properName: "Factors affecting electrical costs", locked: false),
        Xtra(fileName: "LED-Digital-Billboard-Color-Bit-Depth", properName: "LED Digital Billboard Color Bit Depth", locked: false),
        Xtra(fileName: "The-Truth-about-Power-Specs", properName: "The Truth about Power Specs", locked: false),
        Xtra(fileName: "Creative-Guideliness-for-LED-Digital-Billbaords", properName: "Creative Guideliness for LED Digital Billbaords", locked: true),
        Xtra(fileName: "DBB-Data-Connections-for-your-Digital", properName: "DBB Data Connections for your Digital", locked: true),
        Xtra(fileName: "DBB-Intelligent-Safe-Guards", properName: "DBB Intelligent Safe Guards", locked: false),
        Xtra(fileName: "Ready-to-Advertise-Using-Digital", properName: "Ready to Advertise Using Digital", locked: true),
        Xtra(fileName: "Formetcos-Control-Center", properName: "Formetcos Control Center", locked: true)
    ]*/
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let xtra = xtras[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PDFCell", for: indexPath) as! XtrasCollectionViewCell
        if (xtra.locked == true){
            if (ModelManager.sharedInstance.isLoggedIn()){
                cell.pdfLock.isHidden = true
            }else{
                cell.pdfLock.isHidden = false
            }
        }
        cell.modalView = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
        cell.loginModalView = self.storyboard!.instantiateViewController(withIdentifier: "loginview") as? LoginViewController
        cell.mainView = self
        cell.filename = xtra.fileName
        cell.pdfLabel.text = xtra.properName
        ImageLoader.sharedLoader.imageForUrl("http://www.formetco.com/app/xtras/thumbnails/" + xtra.fileName + ".jpg", completionHandler:{(image: UIImage?, url: String) in
            cell.pdfThumbnail.image = image
            cell.pdfProgressBar.stopAnimating()
            cell.pdfProgressBar.isHidden = true
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return xtras.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}

@available(iOS 8.0, *)
class XtrasTableViewiPhone : UITableViewController{
    
    var xtras : [Xtra] = [
        Xtra(fileName: "Digital-Billboards-Deliver-Critical-Information", properName: "Digital Billboards Deliver Critical Information", locked: false),
        Xtra(fileName: "Factors-affecting-electrical-costs", properName: "Factors affecting electrical costs", locked: false),
        Xtra(fileName: "LED-Digital-Billboard-Color-Bit-Depth", properName: "LED Digital Billboard Color Bit Depth", locked: false),
        Xtra(fileName: "The-Truth-about-Power-Specs", properName: "The Truth about Power Specs", locked: false),
        Xtra(fileName: "Creative-Guideliness-for-LED-Digital-Billbaords", properName: "Creative Guideliness for LED Digital Billbaords", locked: true),
        Xtra(fileName: "DBB-Data-Connections-for-your-Digital", properName: "DBB Data Connections for your Digital", locked: true),
        Xtra(fileName: "DBB-Intelligent-Safe-Guards", properName: "DBB Intelligent Safe Guards", locked: false),
        Xtra(fileName: "Ready-to-Advertise-Using-Digital", properName: "Ready to Advertise Using Digital", locked: true)
    ]
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) 
            return cell
        }else{
            
            let xtra = xtras[indexPath.row - 1]
            let cell : XtrasTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PDFCell", for: indexPath) as! XtrasTableViewCell
            if (xtra.locked == true){
                if (ModelManager.sharedInstance.isLoggedIn()){
                    cell.pdfLock.isHidden = true
                }else{
                    cell.pdfLock.isHidden = false
                }
            }
            cell.modalView = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
            cell.loginModalView = self.storyboard!.instantiateViewController(withIdentifier: "loginview") as? LoginViewController
            cell.mainView = self
            cell.filename = xtra.fileName
            cell.pdfLabel.text = xtra.properName
            ImageLoader.sharedLoader.imageForUrl("http://www.formetco.com/app/xtras/thumbnails/" + xtra.fileName + ".jpg", completionHandler:{(image: UIImage?, url: String) in
                cell.pdfThumbnail.image = image
                cell.pdfProgressBar.stopAnimating()
                cell.pdfProgressBar.isHidden = true
            })
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return xtras.count + 1
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0){
            return 122
        }else{
            return 321
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0){
            return 122
        }else{
            return 321
        }
    }
}

class XtrasNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XtrasViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return UIInterfaceOrientation.portrait
        }else{
            return UIInterfaceOrientation.landscapeRight
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    @IBAction func showFTXWebsite(_ sender: AnyObject) {
        let filename : String = "http://www.formetco.com/ftx"
        let link = filename.addingPercentEscapes(using: String.Encoding.utf8)! as String!
        UIApplication.shared.openURL(URL(string: String(describing: link))!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.isMovingFromParentViewController || self.isBeingDismissed){
            self.dispose()
        }
    }
    
    deinit {
        self.dispose()
    }
}

@available(iOS 8.0, *)
class XtrasCollectionViewCell : UICollectionViewCell, UINavigationControllerDelegate
{
    var modalView : FXWebView?
    var loginModalView : LoginViewController?
    var mainView : UIViewController?
    @IBOutlet weak var pdfProgressBar: UIActivityIndicatorView!
    @IBOutlet weak var pdfThumbnail: UIImageView!
    @IBOutlet weak var pdfLock: UIImageView!
    @IBOutlet weak var pdfLabel: UILabel!
    var filename : String = ""
    var navigationController : UINavigationController?
    
    @IBAction func overlayButtonClicked(_ sender: AnyObject) {
        /*Open pdf file*/
        autoreleasepool {
            if (ModelManager.sharedInstance.isLoggedIn() == false && !pdfLock.isHidden){
                ModelManager.sharedInstance.fromHome = false
                /*Present login, then after dismissed open pdf file if they're then logged in*/
                let loginViewController = loginModalView
                let navigationController = UINavigationController(rootViewController: loginViewController!)
                navigationController.delegate = self
                //var navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XtrasCollectionViewCell.dismissView(_:)))
                navigationController.navigationBar.barStyle = UIBarStyle.black
                navigationController.navigationBar.isTranslucent = true
                navigationController.navigationBar.topItem?.title = pdfLabel.text
                navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
                //loginViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
                self.mainView!.present(navigationController, animated: true, completion: nil)
            }else{
                NSLog("Open PDF File")
                let filename_ : String = "http://www.formetco.com/app/xtras/downloads/" + filename + ".pdf"
                let link = filename_.addingPercentEscapes(using: String.Encoding.utf8)! as String!
        
                let webViewController = modalView

                let navigationController = UINavigationController(rootViewController: webViewController!)
                navigationController.delegate = self
                let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XtrasCollectionViewCell.dismissView(_:)))
                navigationControllerExitBtn.tintColor = UIColor.white
                navigationController.navigationBar.barStyle = UIBarStyle.black
                navigationController.navigationBar.isTranslucent = false
                navigationController.navigationBar.tintColor = UIColor.black
                navigationController.navigationBar.topItem?.title = pdfLabel.text
                navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
                let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
                webView.scalesPageToFit=true
                let request = URLRequest(url: URL(string: link!)!)
                webView.loadRequest(request)
                webViewController?.view.addSubview(webView)
                navigationController.view.addSubview(webView)
                webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
                self.mainView!.present(navigationController, animated: true, completion: nil)
            }
        }
    }
    
    func dismissView(_ sender: AnyObject?){
        print("Called dismiss", terminator: "")
        self.loginModalView?.dismiss(animated: true, completion: nil)
        self.modalView?.dismiss(animated: true, completion: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}


@available(iOS 8.0, *)
class XtrasTableViewCell : UITableViewCell, UINavigationControllerDelegate
{
    var modalView : FXWebView?
    var loginModalView : LoginViewController?
    var mainView : UIViewController?
    @IBOutlet weak var pdfProgressBar: UIActivityIndicatorView!
    @IBOutlet weak var pdfThumbnail: UIImageView!
    @IBOutlet weak var pdfLock: UIImageView!
    @IBOutlet weak var pdfLabel: UILabel!
    var filename : String = ""
    var navigationController : UINavigationController?
    
    @IBAction func overlayButtonClicked(_ sender: AnyObject) {
        /*Open pdf file*/
        autoreleasepool {
            if (ModelManager.sharedInstance.isLoggedIn() == false && !pdfLock.isHidden){
                ModelManager.sharedInstance.fromHome = false
                /*Present login, then after dismissed open pdf file if they're then logged in*/
                let loginViewController = loginModalView
                let navigationController = UINavigationController(rootViewController: loginViewController!)
                navigationController.delegate = self
                //var navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XtrasCollectionViewCell.dismissView(_:)))
                navigationController.navigationBar.barStyle = UIBarStyle.black
                navigationController.navigationBar.isTranslucent = true
                navigationController.navigationBar.topItem?.title = pdfLabel.text
                navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
                //loginViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
                self.mainView!.present(navigationController, animated: true, completion: nil)
            }else{
                NSLog("Open PDF File")
                let filename_ : String = "http://www.formetco.com/app/xtras/downloads/" + filename + ".pdf"
                let link = filename_.addingPercentEscapes(using: String.Encoding.utf8)! as String!
                
                let webViewController = modalView
                
                let navigationController = UINavigationController(rootViewController: webViewController!)
                navigationController.delegate = self
                let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XtrasCollectionViewCell.dismissView(_:)))
                navigationControllerExitBtn.tintColor = UIColor.white
                navigationController.navigationBar.barStyle = UIBarStyle.black
                navigationController.navigationBar.isTranslucent = false
                navigationController.navigationBar.tintColor = UIColor.black
                navigationController.navigationBar.topItem?.title = pdfLabel.text
                navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
                let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
                webView.scalesPageToFit=true
                let request = URLRequest(url: URL(string: link!)!)
                webView.loadRequest(request)
                webViewController?.view.addSubview(webView)
                navigationController.view.addSubview(webView)
                webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
                self.mainView!.present(navigationController, animated: true, completion: nil)
            }
        }
    }
    
    func dismissView(_ sender: AnyObject?){
        print("Called dismiss", terminator: "")
        self.loginModalView?.dismiss(animated: true, completion: nil)
        self.modalView?.dismiss(animated: true, completion: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}
