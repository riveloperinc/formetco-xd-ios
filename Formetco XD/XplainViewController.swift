//
//  XplainViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 4/26/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class XplainViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
    
    @IBOutlet weak var xplainCollectionView: UICollectionView!
    override func viewWillDisappear(_ animated: Bool) {
        print("Deinit should have been called.")
        //xplainCollectionView.delegate = nil
    }
    
    @IBAction func goBack(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        print("Deinit called on xplain")
    }
    
    var showFTXAlertView = UIAlertView()
    @IBAction func showFTX(_ sender: AnyObject){
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let device : UIDevice = UIDevice.current
            let systemVersion = (device.systemVersion as NSString).floatValue
            if(systemVersion < 8.0) {
                let alert = showFTXAlertView
                alert.title = "Contact Us"
                alert.message = "Dial +1 (800) 204-4386?"
                alert.addButton(withTitle: "Call")
                alert.addButton(withTitle: "Cancel")
                alert.show()
                alert.delegate = self
            }else{
                let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("Call Pressed")
                    UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (alertView == showFTXAlertView){
            if (buttonIndex == 0){
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
        }
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    var Pages : [XplainPage] = ModelManager.sharedInstance.getXplainPages()
    /*[
        XplainPage(ID: 1, XplainVideos:
        [
            Xplain(ID: 0, youtubeTitle: "F4X DIS Card", youtubeDescription: "", youtubeID: "qXKpD49ID6k"),
            Xplain(ID: 1, youtubeTitle: "F4X Cabinet Introduction", youtubeDescription: "", youtubeID: "EOHUGaQLb8U"),
            Xplain(ID: 2, youtubeTitle: "Secondary PC Swap", youtubeDescription: "", youtubeID: "PESUqEY13YY"),
            Xplain(ID: 3, youtubeTitle: "F4X Module", youtubeDescription: "One of our Xperts explains the simple change out.", youtubeID: "QD-5rzh56sM"),
            Xplain(ID: 4, youtubeTitle: "F4X Power Supply Change", youtubeDescription: "The simple replacement of the F4X power supply can be performed in under a minute.", youtubeID: "C0OOsPuJWB4"),
            Xplain(ID: 5, youtubeTitle: "Changing your camera", youtubeDescription: "", youtubeID: "CjYlce1IiSc")
        ]),
        
        XplainPage(ID: 2, XplainVideos:
        [
            Xplain(ID: 6, youtubeTitle: "EMC Temp Probe Swap", youtubeDescription: "", youtubeID: "gjVNf7RK114"),
            Xplain(ID: 7, youtubeTitle: "EMC Photocell Change", youtubeDescription: "", youtubeID: "CYoXvcyOroc"),
            Xplain(ID: 8, youtubeTitle: "Digital Billboard Control Box", youtubeDescription: "", youtubeID: "u5OCg2MNFI4"),
            Xplain(ID: 9, youtubeTitle: "Intellgent Safeguards", youtubeDescription: "Our digital billboards deliver the ultimate peace of mind.", youtubeID: "GMeSq-pU4Rw"),
            Xplain(ID: 10, youtubeTitle: "FTX Reciever Card", youtubeDescription: "The simple replacement of the F4X power supply can be performed in under a minute.", youtubeID: "C2iAMhhg6eQ"),
            Xplain(ID: 11, youtubeTitle: "DBB CPS 100 Overview", youtubeDescription: "", youtubeID: "jJTJD3YTsbs")
        ]),
        
        XplainPage(ID: 3, XplainVideos:
        [
            Xplain(ID: 12, youtubeTitle: "Braves discuss their F4X board", youtubeDescription: "", youtubeID: "oSIQcr8fZdM"),
            Xplain(ID: 13, youtubeTitle: "FTX Power Supply Change", youtubeDescription: "", youtubeID: "51SxPI0mDZs"),
            Xplain(ID: 14, youtubeTitle: "Secondary PC Swap", youtubeDescription: "", youtubeID: "yeW5I64V_dI"),
            Xplain(ID: 15, youtubeTitle: "FTX Module Rear Change", youtubeDescription: "Simple module change from the rear.", youtubeID: "rt4onYejPA8"),
            Xplain(ID: 16, youtubeTitle: "FTX Module Change", youtubeDescription: "Watch the simple swap of an FTX module.", youtubeID: "LERJ-fumy90"),
            Xplain(ID: 17, youtubeTitle: "FTX Install", youtubeDescription: "", youtubeID: "UMcBXR1EwWU")
        ])
    ]*/
    
    var Videos : [Xplain] = ModelManager.sharedInstance.xplainVideos
    
    @IBOutlet weak var errorContainerView: UIView!
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XplainViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
        
        if (Reachability.isConnectedToNetwork() == false){
            errorContainerView.isHidden = false
        }
        
        self.moveDown()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var page : XplainPage = Pages[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YoutubeResponsiveCell", for: indexPath) as! XplainCollectionViewPage
        cell.modalView = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
        cell.mainView = self
        
        cell.xplains = page.XplainYoutubeVideos

        if (page.XplainYoutubeVideos[0].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[0].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockOneViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockOneViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockOneViewProgressBar.stopAnimating()
                cell.xplainBlockOneViewProgressBar.isHidden = true
            }
        })
        
        
        cell.xplainBlockOneViewButtonTitle.text = page.XplainYoutubeVideos[0].youtubeTitle
        }
        
        if (page.XplainYoutubeVideos[1].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[1].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockTwoViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockTwoViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockTwoViewProgressBar.stopAnimating()
                cell.xplainBlockTwoViewProgressBar.isHidden = true
            }
        })
        
        cell.xplainBlockTwoViewButtonTitle.text = page.XplainYoutubeVideos[1].youtubeTitle
        }
        
        if (page.XplainYoutubeVideos[2].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[2].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockThreeViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockThreeViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockThreeViewProgressBar.stopAnimating()
                cell.xplainBlockThreeViewProgressBar.isHidden = true
            }
        })
        
        cell.xplainBlockThreeViewButtonTitle.text = page.XplainYoutubeVideos[2].youtubeTitle
        }
        
        if (page.XplainYoutubeVideos[3].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[3].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockFourViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockFourViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockFourViewProgressBar.stopAnimating()
                cell.xplainBlockFourViewProgressBar.isHidden = true
            }
        })
        
        cell.xplainBlockFourViewButtonTitle.text = page.XplainYoutubeVideos[3].youtubeTitle
        cell.xplainBlockFourViewButtonDescription.text = page.XplainYoutubeVideos[3].youtubeDescription
        }
        
        if (page.XplainYoutubeVideos[4].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[4].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockFiveViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockFiveViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockFiveViewProgressBar.stopAnimating()
                cell.xplainBlockFiveViewProgressBar.isHidden = true
            }
        })
        
        cell.xplainBlockFiveViewButtonTitle.text = page.XplainYoutubeVideos[4].youtubeTitle
        cell.xplainBlockFiveViewButtonDescription.text = page.XplainYoutubeVideos[4].youtubeDescription
        }
        
        
        if (page.XplainYoutubeVideos[5].ID) != nil {
        ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(page.XplainYoutubeVideos[5].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
            autoreleasepool {
                cell.xplainBlockSixViewButton.setBackgroundImage(image, for: UIControlState())
                cell.xplainBlockSixViewButton.setBackgroundImage(image, for: UIControlState.highlighted)
                cell.xplainBlockSixViewProgressBar.stopAnimating()
                cell.xplainBlockSixViewProgressBar.isHidden = true
            }
        })
        
        cell.xplainBlockSixViewButtonTitle.text = page.XplainYoutubeVideos[5].youtubeTitle
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pages.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) 
            return cell
        }else if(indexPath.row == 1){
            let id = indexPath.row - 1
            //Show showcase view.
            let cell : XplainShowcaseCell = tableView.dequeueReusableCell(withIdentifier: "showcaseCell", for: indexPath) as! XplainShowcaseCell
            cell.modalView = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
            cell.mainView = self
            cell.xplain = Videos[id]
            //cell.layer.borderColor = UIColor.redColor().CGColor
            //cell.layer.borderWidth = 2
            cell.showcaseTitle.text = String(Videos[id].youtubeTitle)
            ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(Videos[id].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
                cell.showcaseActivityBar.isHidden = true
                cell.showcaseActivityBar.stopAnimating()
                autoreleasepool {
                    cell.showcaseImageView.image = image
                }
            })
            return cell
        }else{
            let id = indexPath.row - 1
            let cell : XplainNormalCell = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! XplainNormalCell
            cell.modalView = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
            cell.mainView = self
            cell.xplain = Videos[id]
            cell.normalTitle.text = String(Videos[id].youtubeTitle)
            ImageLoader.sharedLoader.imageForUrl("http://f4xdigitalbillboards.com/app/xd/xplain_images/" + String(Videos[id].ID) + ".png", completionHandler:{(image: UIImage?, url: String) in
                cell.normalActivityBar.isHidden = true
                cell.normalActivityBar.stopAnimating()
                autoreleasepool {
                    cell.normalImageView.image = image
                }
            })
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Cell selected", terminator: "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (Device() == .iPhone4 || Device() == .iPhone4s){
            if (indexPath.row == 0){
                return 76
            }else if(indexPath.row == 1){
                return 257
            }else{
                return 114
            }
        }else if (Device() == .iPhone5 || Device() == .iPhone5s){
            if (indexPath.row == 0){
                return 76
            }else if(indexPath.row == 1){
                return 237
            }else{
                return 85
            }
        }else if (Device() == .iPhone6 || Device() == .iPhone6){
            if (indexPath.row == 0){
                return 76
            }else if(indexPath.row == 1){
                return 270
            }else{
                return 114
            }
        }else{
            if (indexPath.row == 0){
                return 76
            }else if(indexPath.row == 1){
                return 257
            }else{
                return 114
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Videos.count + 1
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        self.gotoHome()
    }
    
    func gotoHome(){
        autoreleasepool {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = ModelManager.storyboard
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "homenavview") as! UINavigationController
            appdelegate.window!.rootViewController = nav
        }
    }
}

class XplainNavigationController : UINavigationController{
    override func viewDidLoad() {
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(XplainViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView
    }
    
    func gotoHome(){
        let homeNavViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! HomeNavigationController
        
        self.navigationController?.present(homeNavViewController, animated: true, completion: nil)
        
        self.dispose()
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.dispose()
    }
}

class XplainCollectionViewPage : UICollectionViewCell, UINavigationControllerDelegate{
    var xplains : [Xplain] = []
    @IBOutlet weak var xplainBlockOneViewButton : UIButton!
    @IBOutlet weak var xplainBlockOneViewProgressBar: UIActivityIndicatorView!
    @IBOutlet weak var xplainBlockOneViewButtonTitle : UILabel!
    
    @IBOutlet weak var xplainBlockTwoViewButton : UIButton!
    @IBOutlet weak var xplainBlockTwoViewButtonTitle : UILabel!
    @IBOutlet weak var xplainBlockTwoViewProgressBar: UIActivityIndicatorView!
    
    @IBOutlet weak var xplainBlockThreeViewButton : UIButton!
    @IBOutlet weak var xplainBlockThreeViewButtonTitle : UILabel!
    @IBOutlet weak var xplainBlockThreeViewProgressBar: UIActivityIndicatorView!
    
    @IBOutlet weak var xplainBlockFourViewButton : UIButton!
    @IBOutlet weak var xplainBlockFourViewButtonTitle : UILabel!
    @IBOutlet weak var xplainBlockFourViewButtonDescription: UILabel!
    @IBOutlet weak var xplainBlockFourViewProgressBar: UIActivityIndicatorView!
    
    @IBOutlet weak var xplainBlockFiveViewButton : UIButton!
    @IBOutlet weak var xplainBlockFiveViewButtonTitle : UILabel!
    @IBOutlet weak var xplainBlockFiveViewButtonDescription: UILabel!
    @IBOutlet weak var xplainBlockFiveViewProgressBar: UIActivityIndicatorView!
    
    @IBOutlet weak var xplainBlockSixViewButton : UIButton!
    @IBOutlet weak var xplainBlockSixViewButtonTitle : UILabel!
    @IBOutlet weak var xplainBlockSixViewProgressBar: UIActivityIndicatorView!
    
    var modalView : FXWebView?
    weak var mainView : UIViewController?
    weak var navigationController : UINavigationController?
    
    @IBAction func gotoYoutubeVideoClicked(_ sender: AnyObject) {
        autoreleasepool {
            let button : UIButton = sender as! UIButton
            let xplain : Xplain = xplains[button.tag]
            let filename_ : String = "https://www.youtube.com/embed/" + xplain.youtubeID + "?autoplay=1&vq=hd1080"
            print(filename_, terminator: "")
            let link = filename_.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            //UIApplication.sharedApplication().openURL(NSURL(string: String(link))!)
        
            let webViewController = modalView
            let navigationController = UINavigationController(rootViewController: webViewController!)
            navigationController.delegate = self
            let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XplainCollectionViewPage.dismissView(_:)))
            navigationControllerExitBtn.tintColor = UIColor.white
            navigationController.navigationBar.barStyle = UIBarStyle.black
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = UIColor.black
            navigationController.navigationBar.topItem?.title = "Xplain"
            navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
            let request = URLRequest(url: URL(string: link!)!)
            webView.allowsInlineMediaPlayback = false
            webView.mediaPlaybackAllowsAirPlay = false
            webView.mediaPlaybackRequiresUserAction = false
            webView.scalesPageToFit = true
            webView.loadRequest(request)
            webViewController?.view.addSubview(webView)
            navigationController.view.addSubview(webView)
            webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
            self.mainView!.present(navigationController, animated: true, completion: nil)
        }
        
    }
    
    func dismissView(_ sender: AnyObject?){
        print("Called dismiss", terminator: "")
        self.modalView?.dismiss(animated: true, completion: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func supportedInterfaceOrientations() -> Int {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return Int(UIInterfaceOrientationMask.landscape.rawValue)
        }else{
            return Int(UIInterfaceOrientationMask.portrait.rawValue)
        }
    }
    
    func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone){
            return UIInterfaceOrientation.portrait
        }else{
            return UIInterfaceOrientation.landscapeLeft
        }
    }
    
    func shouldAutorotate() -> Bool {
        return false
    }
}

class XplainShowcaseCell : UITableViewCell, UINavigationControllerDelegate {
    @IBOutlet weak var showcaseTitle: UILabel!
    @IBOutlet weak var showcaseImageView: UIImageView!
    @IBOutlet weak var showcaseActivityBar: UIActivityIndicatorView!
    
    var modalView : FXWebView?
    var xplain : Xplain?
    weak var mainView : UIViewController?
    weak var navigationController : UINavigationController?
    
    @IBAction func gotoYoutubeVideoClicked(_ sender: AnyObject) {
        autoreleasepool {
            //var button : UIButton = sender as! UIButton
            let filename_ : String = "https://www.youtube.com/embed/" + xplain!.youtubeID + "?autoplay=1&vq=hd1080"
            print(filename_, terminator: "")
            let link = filename_.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            //UIApplication.sharedApplication().openURL(NSURL(string: String(link))!)
        
            let webViewController = modalView
            let navigationController = UINavigationController(rootViewController: webViewController!)
            navigationController.delegate = self
            let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XplainCollectionViewPage.dismissView(_:)))
            navigationControllerExitBtn.tintColor = UIColor.white
            navigationController.navigationBar.barStyle = UIBarStyle.black
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = UIColor.black
            navigationController.navigationBar.topItem?.title = "Xplain"
            navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
            let request = URLRequest(url: URL(string: link!)!)
            webView.allowsInlineMediaPlayback = false
            webView.mediaPlaybackAllowsAirPlay = false
            webView.mediaPlaybackRequiresUserAction = false
            webView.scalesPageToFit = true
            webView.loadRequest(request)
            webViewController?.view.addSubview(webView)
            navigationController.view.addSubview(webView)
            webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
            self.mainView!.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func dismissView(_ sender: AnyObject?){
        print("Called dismiss", terminator: "")
        self.modalView?.dismiss(animated: true, completion: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

class XplainNormalCell : UITableViewCell, UINavigationControllerDelegate {
    @IBOutlet weak var normalImageView: UIImageView!
    @IBOutlet weak var normalTitle: UILabel!
    @IBOutlet weak var normalActivityBar: UIActivityIndicatorView!
    
    var modalView : FXWebView?
    var xplain : Xplain?
    weak var mainView : UIViewController?
    weak var navigationController : UINavigationController?
    
    @IBAction func gotoYoutubeVideoClicked(_ sender: AnyObject) {
        autoreleasepool {
            //var button : UIButton = sender as! UIButton
            let filename_ : String = "https://www.youtube.com/embed/" + xplain!.youtubeID + "?autoplay=1&vq=hd1080"
            print(filename_, terminator: "")
            let link = filename_.addingPercentEscapes(using: String.Encoding.utf8)! as String!
            //UIApplication.sharedApplication().openURL(NSURL(string: String(link))!)
            
            let webViewController = modalView
            let navigationController = UINavigationController(rootViewController: webViewController!)
            navigationController.delegate = self
            let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(XplainCollectionViewPage.dismissView(_:)))
            navigationControllerExitBtn.tintColor = UIColor.white
            navigationController.navigationBar.barStyle = UIBarStyle.black
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = UIColor.black
            navigationController.navigationBar.topItem?.title = "Xplain"
            navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
            let request = URLRequest(url: URL(string: link!)!)
            webView.allowsInlineMediaPlayback = false
            webView.mediaPlaybackAllowsAirPlay = false
            webView.mediaPlaybackRequiresUserAction = false
            webView.scalesPageToFit = true
            webView.loadRequest(request)
            webViewController?.view.addSubview(webView)
            navigationController.view.addSubview(webView)
            webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
            self.mainView!.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func dismissView(_ sender: AnyObject?){
        print("Called dismiss", terminator: "")
        self.modalView?.dismiss(animated: true, completion: nil)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
