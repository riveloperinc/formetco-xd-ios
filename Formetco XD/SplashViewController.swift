//
//  SplashViewController.swift
//  Formetco XD
//
//  Created by Austin Sweat on 6/11/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import Foundation
import UIKit

class SplashNavigationController : UINavigationController{
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}

class SplashViewController : UIViewController{
    override func viewDidLoad(){
        let url : String = "http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=init&version=2"
        
        
        let request = NSMutableURLRequest(url: NSURL(string: url) as! URL)
        let session = URLSession.shared
        request.url = URL(string: url)
        request.httpMethod = "GET"
        
        
        
        let task = session.dataTask(with: URL(string: url)!, completionHandler: {data, response, error -> Void in
            
            if data != nil { 
                var json : JSON = JSON(data: data!)
                let xtras_ : [JSON] = (json["xtras"] as JSON).arrayValue
                //var xpand_ : JSON = json["xpand"]
                let xamples_ : [JSON] = (json["xamples"] as JSON).arrayValue
                ModelManager.sharedInstance.xamples = xamples_ //Hotfix
                let xplain_ : [JSON] = (json["xplain"] as JSON).arrayValue
                
                
                for xplain in xplain_ {
                    let id = xplain["ID"].intValue - 1
                    let youtubeID = xplain["youtubeID"].stringValue
                    let youtubeDescription = xplain["youtubeDescription"].stringValue
                    let youtubeTitle = xplain["youtubeTitle"].stringValue
                    
                    let x : Xplain = Xplain(ID: id, youtubeTitle: youtubeTitle, youtubeDescription: youtubeDescription, youtubeID: youtubeID)
                    
                    ModelManager.sharedInstance.xplainVideos.append(x)
                }
                
                for xtra in xtras_ {
                    let properName = xtra["properName"].stringValue
                    let fileName = xtra["fileName"].stringValue
                    let locked = xtra["locked"].boolValue
                    
                    let x : Xtra = Xtra(fileName: fileName, properName: properName, locked: locked)
                    
                    ModelManager.sharedInstance.xtras.append(x)
                }
                
                /*var xTemp = []
                for xample in xamples_ {
                    var tmp = []
                    var xampleTitle = xample[0].stringValue
                    var xampleLinks : [JSON] = xample[1].arrayValue
                    for xampleLink in xampleLinks {
                        var xLink = xampleLink["link"]
                        
                    }
                    break
                }*/
                ModelManager.sharedInstance.xamples = xamples_

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    self.gotoHome()
                })
            }
        })
        task.resume()
        
        
        /*autoreleasepool {
            let seconds = 3.0
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            var dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                self.gotoHome()
            })
        }*/
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view = nil
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    func gotoHome(){
        autoreleasepool {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = ModelManager.storyboard
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "homenavview") as! UINavigationController
            appdelegate.window!.rootViewController = nav
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}
