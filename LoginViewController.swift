//
//  LoginViewController.swift
//  Formetco
//
//  Created by Austin Sweat on 30/01/2015.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class LoginViewController: UIViewController {
    
    var redirect : String = ""
    let redColor = UIColor(red: 195.0/255.0, green: 20.0/255.0, blue: 30.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var backgroundView: UIImageView!
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.contentMode = UIViewContentMode.scaleAspectFill
        //backgroundView.image = UIImage(named: "loginBackground")
        let gesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.viewTapped))
        self.view.addGestureRecognizer(gesture)
        //let modelmanager = ModelManager.sharedInstance
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
        let logoView = UIImageView(image: UIImage(named: "Logo")!)
        logoView.isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target:self, action:#selector(LoginViewController.gotoHome))
        logoView.addGestureRecognizer(tgr)
        self.navigationItem.titleView = logoView;
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginViewController.dismissView(_:)))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "RightIcon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginViewController.gotoXpert))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        /*if(cart.tableCart.count > 0){
            self.navigationItem.rightBarButtonItem?.tintColor = redColor
        }*/
        // Do any additional setup after loading the view.
        //var s = Settings()
        //s.getAPIURL()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.barStyle = UIBarStyle.black
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.black
    }
    
    func gotoXpert(){
        let xpertViewController = self.storyboard?.instantiateViewController(withIdentifier: "xpertview") as! XpertViewController
        
        self.navigationController?.pushViewController(xpertViewController, animated: true)
    }
    
    func dismissView(_ sender: AnyObject?){
        if (ModelManager.sharedInstance.fromHome){
            self.gotoHome()
        }else{
            if(ModelManager.sharedInstance.fromLogin){
                self.gotoHome()
            }else{
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func viewTapped() {
        self.view.endEditing(true)
    }
    
    @IBAction func rightSideNavClicked(_ sender: AnyObject) {
        if (UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            gotoXpert()
        }else{
            let alert = UIAlertController(title: "Contact Us", message: "Dial +1 (800) 204-4386?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("Call Pressed")
                UIApplication.shared.openURL(URL(string:"tel:18002044386")!)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func leftSideNavClicked(_ sender: AnyObject) {
        /*if !opened{
            opened = true
            sideBar.showSideBar(opened)
        }else{
            opened = false
            sideBar.showSideBar(opened)
        }*/
        
        if (ModelManager.sharedInstance.fromHome){
            self.gotoHome()
        }else{
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func gotoHome(){
        let navViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! UINavigationController
        self.navigationController?.present(navViewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  LoginTableViewController.swift
//  Formetco
//
//  Created by Austin Sweat on 30/01/2015.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 8.0, *)
class LoginTableViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var forgotbutton: UIButton!
    @IBOutlet weak var rememberme: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userid: UITextField!
    @IBOutlet weak var rememberMeBtn: UIButton!
    var checked : Bool = false
    let checkedChar: String = "\u{2611}"
    let uncheckedChar: String = "\u{2610}"
    
    @IBAction func rememberMeClicked(_ sender: AnyObject) {
        if checked{
            checked = false
            rememberMeBtn.setTitle("", for: UIControlState())
        }else{
            checked = true
            rememberMeBtn.setTitle(checkedChar, for: UIControlState())
        }
    }
    
    @IBOutlet weak var tableViewContainer: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate : AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDelegate.managedObjectContext!
        
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        let results : NSArray = try! context.fetch(request) as NSArray
        if(results.count > 0){
            let result : NSManagedObject = results[results.count-1] as! NSManagedObject //Final result
            userid.text = result.value(forKey: "username") as? String
            password.text = result.value(forKey: "password") as? String
            
            checked = true
            rememberMeBtn.setTitle(checkedChar, for: UIControlState())
        }
        
        password.layer.cornerRadius = 5.0
        userid.layer.cornerRadius = 5.0
        loginbutton.layer.cornerRadius = 5.0
        forgotbutton.layer.cornerRadius = 5.0
        rememberme.layer.cornerRadius = 5.0
        //password.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        
        let backgroundView : UIView = UIView(frame: CGRect.zero)
        backgroundView.backgroundColor = UIColor.clear
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.backgroundView = backgroundView
        self.tableView.cellForRow(at: IndexPath(index: 0))?.contentView.backgroundColor = UIColor.clear
        self.tableView.cellForRow(at: IndexPath(index: 0))?.contentView.layer.cornerRadius = 5.0
        self.tableView.cellForRow(at: IndexPath(index: 0))?.backgroundView = backgroundView
        self.tableView.cellForRow(at: IndexPath(index: 1))?.contentView.backgroundColor = UIColor.clear
        self.tableView.cellForRow(at: IndexPath(index: 1))?.backgroundView = backgroundView
        self.tableView.cellForRow(at: IndexPath(index: 2))?.contentView.backgroundColor = UIColor.clear
        self.tableView.cellForRow(at: IndexPath(index: 2))?.backgroundView = backgroundView
        self.tableView.cellForRow(at: IndexPath(index: 3))?.contentView.backgroundColor = UIColor.clear
        self.tableView.cellForRow(at: IndexPath(index: 3))?.backgroundView = backgroundView
        self.tableView.cellForRow(at: IndexPath(index: 4))?.contentView.backgroundColor = UIColor.clear
        self.tableView.cellForRow(at: IndexPath(index: 4))?.backgroundView = backgroundView
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "policyviewsegue"{
            return false
        }else{
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*Make API Call*/
        //let newVC = segue.destinationViewController as UIViewController
        if(self.isBeingDismissed || self.isMovingFromParentViewController){
            self.dispose()
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    @IBAction func policyClicked(_ sender: AnyObject) {
        autoreleasepool {
            print("Policy Clicked")
            let webViewController = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
            let navigationController = UINavigationController(rootViewController: webViewController!)
            let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginViewController.dismissView(_:)))
            navigationControllerExitBtn.tintColor = UIColor.white
            navigationController.navigationBar.barStyle = UIBarStyle.black
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = UIColor.black
            navigationController.navigationBar.topItem?.title = "Formetco Privicy Policy"
            navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
            let request = URLRequest(url: URL(string: "http://f4xdigitalbillboards.com/app/legal/Privacy.html")!)
            webView.loadRequest(request)
            webViewController?.view.addSubview(webView)
            navigationController.view.addSubview(webView)
            webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func _loginClicked(_ sender: AnyObject) {
        autoreleasepool {
            print("Signup Clicked")
            let webViewController = self.storyboard!.instantiateViewController(withIdentifier: "fxwebview") as? FXWebView
            let navigationController = UINavigationController(rootViewController: webViewController!)
            let navigationControllerExitBtn : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Backarrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginViewController.dismissView(_:)))
            navigationControllerExitBtn.tintColor = UIColor.white
            navigationController.navigationBar.barStyle = UIBarStyle.black
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = UIColor.black
            navigationController.navigationBar.topItem?.title = "MyOutdoor"
            navigationController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            let webView = UIWebView(frame: CGRect(x: 0, y: navigationController.navigationBar.bounds.height + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationController.navigationBar.bounds.height))
            let request = URLRequest(url: URL(string: "http://www.formetco.com/account.php")!)
            webView.loadRequest(request)
            webViewController?.view.addSubview(webView)
            navigationController.view.addSubview(webView)
            webViewController!.navigationItem.leftBarButtonItem = navigationControllerExitBtn
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func dismissView(_ sender: AnyObject?){
        self.navigationController?.dismiss(animated: true, completion: nil)
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func deleteLoginEntries(){
        let appDelegate : AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDelegate.managedObjectContext!
        
        let dbrequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Users")
        
        dbrequest.returnsObjectsAsFaults = false
        
        let results : NSArray = try! context.fetch(dbrequest) as NSArray
        if(results.count > 0){
            for r in results{
                let result = r as! NSManagedObject
                context.delete(result)
            }
            do {
                try context.save()
            } catch _ {
            }
        }
    }
    
    func gotoHome(){
        let navViewController = self.storyboard?.instantiateViewController(withIdentifier: "homenavview") as! UINavigationController
        self.navigationController?.present(navViewController, animated: true, completion: nil)
        self.dispose()
    }
    
    @IBAction func loginClicked(_ sender: AnyObject) {
        let loadingView : UIView = UIView(frame: self.view.frame)
        //loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.95)
        //loadingView.layer.cornerRadius = 4
        let loadingIcon : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: ((loadingView.frame.width / 2) - 20), y: ((loadingView.frame.height / 2) - 20), width: 40, height: 40))
        loadingIcon.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.95)
        
        loadingIcon.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        loadingIcon.startAnimating()
        loadingView.isHidden = false
        loadingView.addSubview(loadingIcon)
        self.view.addSubview(loadingView)
        
        let modelmanager = ModelManager.sharedInstance
        
        let email = userid.text!
        let password = self.password.text!
        
        if (checked || rememberMeBtn.titleLabel?.text != "") {
            self.deleteLoginEntries()
            let appDelegate : AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            let context : NSManagedObjectContext = appDelegate.managedObjectContext!
            let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context) 
            newUser.setValue(email, forKey: "username")
            newUser.setValue(password, forKey: "password")
            do {
                try context.save()
            } catch _ {
            }
        }
        
        let url : String = "\(Settings().getAPIUrl())?cmd=login&email="+email+"&password="+password
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if data != nil {
                var allData : JSON = JSON(data: data!)
                var Token : JSON = allData["Token"]
                print(allData[1], terminator: "")
                loadingView.isHidden = true
                if Token.stringValue != "Invalid"{
                    print("Token: \(Token.stringValue)", terminator: "")
                    modelmanager.token = Token.stringValue
                    modelmanager.email = email
                    if (ModelManager.sharedInstance.fromHome){
                        self.gotoHome()
                    }else{
                        /*if(ModelManager.sharedInstance.fromLogin){
                            self.gotoHome()
                        }else{*/
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        //}
                    }
                }else{
                    /*Present error*/
                    let alert = UIAlertController(title: "A-X-ess Denied.", message: "Sorry, please check your login info or try again. If you need support email developer@formetco.com or call (800) 367-6382.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func forgotClicked(_ sender: AnyObject) {
        //let modelmanager = ModelManager.sharedInstance
        
        let email = userid.text!
        
        let url : String = "\(Settings().getAPIUrl())?cmd=resetpassword&version=2&email="+email
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            if data != nil {
                var allData : JSON = JSON(data: data!)
                var Success : JSON = allData["Success"]
                //var Error : JSON = allData["Error"]
                if Success.stringValue != ""{
                    let alert = UIAlertController(title: "Password Reset Sent.", message: "Please check your email for your new password.", preferredStyle:UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Invalid Email!", message: "Please check the form above, and try again.", preferredStyle:UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let backgroundView : UIView = UIView(frame: CGRect.zero)
        backgroundView.backgroundColor = UIColor.clear
        cell.backgroundView = backgroundView
        cell.backgroundColor = UIColor.clear
    }
}

