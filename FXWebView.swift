//
//  FXWebView.swift
//  Formetco XD
//
//  Created by Austin Sweat on 6/3/15.
//  Copyright (c) 2015 Austin Sweat. All rights reserved.
//

import Foundation
import UIKit

class FXWebNavigationController : UINavigationController{
    
    override func viewDidLoad() {

    }
    
    func changeTitle(_ title: String){
        self.navigationController!.navigationBar.topItem?.title = title
    }
    
    func loadWebView(_ webViewController: UIViewController, link: String){
        
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}

class FXWebView : UIViewController{
    @IBOutlet var webview: UIWebView!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var navigationTitleItem: UINavigationItem!
    @IBOutlet var exitBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func closeWebView(_ sender: AnyObject) {
        self.dismissView()
    }
    
    func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    func loadWebView(_ url: String){
        let request: URLRequest = URLRequest(url: URL(string: url)!)
        webview.loadRequest(request)
    }
    
    func changeNavTitle(_ title: String){
        self.navigationTitleItem.title = title
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            return UIInterfaceOrientationMask.landscape
        }else{
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    /*override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    if(UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone){
    return UIInterfaceOrientation.Portrait
    }else{
    return UIInterfaceOrientation.LandscapeRight
    }
    }*/
    
    override var shouldAutorotate : Bool {
        return true
    }
}
